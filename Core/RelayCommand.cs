﻿using System;
using System.Windows.Input;

namespace ZamowieniaPrzyszle.Core
{
    class RelayCommand : ICommand
    {
        //private readonly Action<object> _execute;
        //private readonly Func<object, bool> _canExecute;
        //private object v;

        //public RelayCommand(Action<object> execute)
        //    : this(execute, null)
        //{ }

        //public RelayCommand(Action<object> execute, Func<object, bool> canExecute)
        //{
        //    if (execute is null) throw new ArgumentNullException(nameof(execute));

        //    _execute = execute;
        //    _canExecute = canExecute ?? (x => true);
        //}

        //public RelayCommand(object v)
        //{
        //    this.v = v;
        //}

        //public bool CanExecute(object parameter) => _canExecute(parameter);

        //public void Execute(object parameter) => _execute(parameter);

        //public event EventHandler CanExecuteChanged
        //{
        //    add
        //    {
        //        CommandManager.RequerySuggested += value;
        //    }
        //    remove
        //    {
        //        CommandManager.RequerySuggested -= value;
        //    }
        //}

        //public void Refresh() => CommandManager.InvalidateRequerySuggested();
        private Action<object> action;
        public RelayCommand(Action<object> action)
        {
            this.action = action;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            action(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
