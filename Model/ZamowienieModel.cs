﻿using System;
using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class ZamowienieModel : ViewModelBase
    {
        [SkipExportExcel]
        public decimal IdFirmy { get; set; }
        [SkipExportExcel]
        public decimal IdMagazynu { get; set; }
        [SkipExportExcel]
        public decimal IdZamPrzyszle { get; set; }
        public string Sezon { get; set; }
        public string Numer { get; set; }
        [DisplayName("Data zamówienia")]
        public DateTime DataZamowienia { get; set; }
        [DisplayName("Data realizacji")]
        public DateTime DataRealizacji { get; set; }
        [SkipExportExcel]
        public decimal IdFabryki { get; set; }
        public string Fabryka { get; set; }
        [DisplayName("Wartość netto")]
        public decimal WartoscNetto { get; set; }
        [DisplayName("Wartość brutto")]
        public decimal WartoscBrutto { get; set; }
        public string Waluta { get; set; }
        [DisplayName("Wartość netto waluta")]
        public decimal WartoscNettoWal { get; set; }
        [DisplayName("Wartość brutto waluta")]
        public decimal WartoscBruttoWal { get; set; }
    }
}
