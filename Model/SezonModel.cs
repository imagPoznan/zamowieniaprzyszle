﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class SezonModel : ViewModelBase
    {
        public string Sezon { get; set; }
        [SkipExportExcel]
        public decimal IdFabryki { get; set; }
        [SkipExportExcel]
        public string Fabryka { get; set; }
        [SkipExportExcel]
        public decimal IdArtykulu { get; set; }
        [DisplayName("Indeks handlowy")]
        public string IndeksHandlowy { get; set; }
        [DisplayName("Nazwa artykułu")]
        public string Nazwa { get; set; }
        [DisplayName("Kolor")]
        public string Kolor { get; set; }
        [DisplayName("Numer koloru")]
        public string NumerKoloru { get; set; }
        public string Pakowanie { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get; set; }
        public decimal Dostarczono { get; set; }
        public decimal Zarezerwowano { get; set; }
        [DisplayName("Pozostało")]
        public decimal Pozostalo { get; set; }
    }
}
