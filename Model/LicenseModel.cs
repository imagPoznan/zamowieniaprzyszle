﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class LicenseModel : ViewModelBase
    {
        public enum LicencjaStatus { Potwierdzona, Brak, Tymczasowa }
        private bool _licencjaAktywna;
        private LicencjaStatus _licencjaStan;
        public static LicenseModel Instance { get; set; } = new LicenseModel();
        public SettingsModel SettingsModel { get; set; } = new SettingsModel();
        public string LicencjaNumer { get => SettingsModel.Instance.LicencjaNumer; set => SettingsModel.Instance.LicencjaNumer = value; }
        public LicencjaStatus LicencjaStan { get => _licencjaStan; set { _licencjaStan = value; OnPropertyChanged("LicencjaStan"); } }
        public DateTime? LicencjaWygasa { get; set; }
        public bool LicencjaAktywna { get => _licencjaAktywna; set { if (_licencjaAktywna == value) { return; } _licencjaAktywna = value; OnPropertyChanged("LicencjaAktywna"); } }
        public string LicencjaOpis { get; set; }
    }
}
