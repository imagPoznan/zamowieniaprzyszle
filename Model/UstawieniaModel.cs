﻿using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle
{
    public class UstawieniaModel : ViewModelBase
    {
        #region UstawieniaSQL
        private string _sqlServer;
        private string _sqlDatabase;
        private List<string> _auth = new List<string>()
        {
            "Windows Authentication",
            "SQL Server Authentication"
        };
        private string _sqlUser;
        private string _sqlPassword;
        private string _dataWygasnieciaLicencji;
        public List<string> Auth
        {
            get { return _auth; }
            set
            {
                _auth = value;
                OnPropertyChanged(() => Auth);
            }
        }
        public string SqlServer
        {
            get { return _sqlServer; }
            set
            {
                _sqlServer = value;
                OnPropertyChanged(() => SqlServer);
            }
        }
        public string SqlDatabase
        {
            get { return _sqlDatabase; }
            set
            {
                _sqlDatabase = value;
                OnPropertyChanged(() => SqlDatabase);
            }
        }
        public List<string> SqlAuth
        {
            get { return _auth; }
            set
            {
                _auth = value;
                OnPropertyChanged(() => SqlAuth);
            }
        }
        public string SqlUser
        {
            get { return _sqlUser; }
            set
            {
                _sqlUser = value;
                OnPropertyChanged(() => SqlUser);
            }
        }
        public string SqlPassword
        {
            get { return _sqlPassword; }
            set
            {
                _sqlPassword = value;
                OnPropertyChanged(() => SqlPassword);
            }
        }
        public string NumerLicencji
        {
            //get { return SettingsHelper.Instance.NumerLicencji; }
            //set
            //{
            //    SettingsHelper.Instance.NumerLicencji = value;
            //}
            get;set;
        }
        public string DataWygasnieciaLicencji
        {
            get { return _dataWygasnieciaLicencji; }
            set
            {
                _dataWygasnieciaLicencji = value;
                OnPropertyChanged(() => DataWygasnieciaLicencji);
            }
        }
        public static string GetSqlConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
        public void SaveSqlConnectionString(string name, string server, string database, string auth, string user, string password)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings.Remove(name);
            config.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings(name, "Server = "+server+"; Database = "+database+"; User Id = "+user+"; Password = "+password+";"));
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");
        }

        public ICommand ZapiszUstawienia;
        //private async Task ZapiszUstawienia()
        //{
        //    try
        //    {
        //        SettingsController.
        //    }
        //    catch (System.Exception)
        //    {

        //        throw;
        //    }
        //}
        #endregion
    }
}
