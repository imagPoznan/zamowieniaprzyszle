﻿using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class ArtykulModel : ViewModelBase
    {
        [SkipExportExcel]
        public decimal IdArtykulu { get; set; }
        [SkipExportExcel]
        [DisplayName("Indeks katalogowy")]
        public string IndeksKatalogowy { get; set; }
        [DisplayName("Indeks handlowy")]
        public string IndeksHandlowy { get; set; }
        [DisplayName("Nazwa artykułu")]
        public string Nazwa { get; set; }
        [DisplayName("Nazwa koloru")]
        public string Kolor { get; set; }
        [DisplayName("Numer koloru")]
        public string NumerKoloru { get; set; }
        public string Pakowanie { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get; set; }
        public decimal Dostarczono { get; set; }
        public decimal Zarezerwowano { get; set; }
        [DisplayName("Pozostało")]
        public decimal Pozostalo { get; set; }
    }
}
