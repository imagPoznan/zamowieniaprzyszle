﻿using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class PozycjaZamowieniaModel : ViewModelBase
    {
        private decimal _zamowiono;
        private decimal _dostarczono;
        private decimal _wartoscNetto;
        private decimal _wartoscBrutto;
        private decimal _wartoscNettoWal;
        private decimal _wartoscBruttoWal;
        private decimal _doRealizacji;
        private bool _doRealizacjiAktywne;
        [DisplayName("Lp.")]
        public int Lp { get; set; }
        [SkipExportExcel]
        public decimal IdZamowienia { get; set; }
        [SkipExportExcel]
        public decimal IdPozZamowienia { get; set; }
        [SkipExportExcel]
        public decimal IdArtykulu { get; set; }
        [DisplayName("Indeks katalogowy")]
        public string IndeksKatalogowy { get; set; }
        [DisplayName("Indeks handlowy")]
        public string IndeksHandlowy { get; set; }
        public string Nazwa { get; set; }
        public string Kolor { get; set; }
        public string NumerKoloru { get; set; }
        public string Pakowanie { get; set; }
        public string Jednostka { get; set; }
        public decimal Przelicznik { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get => _zamowiono / Przelicznik; set => _zamowiono = value; }
        [DisplayName("Cena netto")]
        public decimal CenaNetto { get; set; }
        [DisplayName("Cena brutto")]
        public decimal CenaBrutto { get; set; }
        [DisplayName("Cena netto waluta")]
        public decimal CenaNettoWal { get; set; }
        [DisplayName("Cena brutto waluta")]
        public decimal CenaBruttoWal { get; set; }
        [DisplayName("Wartość netto")]
        public decimal WartoscNetto
        {
            get => _doRealizacji == 0 ? _wartoscNetto : CenaNetto * _doRealizacji;
            set
            {
                _wartoscNetto = value;
                OnPropertyChanged();
            }
        }
        [DisplayName("Wartość brutto")]
        public decimal WartoscBrutto
        {
            get => _doRealizacji == 0 ? _wartoscBrutto : CenaBrutto * _doRealizacji;
            set
            {
                _wartoscBrutto = value;
                OnPropertyChanged();
            }
        }
        [DisplayName("Wartość netto waluta")]
        public decimal WartoscNettoWal
        {
            get => _doRealizacji == 0 ? _wartoscNettoWal : CenaNettoWal * _doRealizacji;
            set
            {
                _wartoscNettoWal = value;
                OnPropertyChanged();
            }
        }

        [DisplayName("Wartość brutto waluta")]
        public decimal WartoscBruttoWal
        {
            get => _doRealizacji == 0 ? _wartoscBruttoWal : CenaBruttoWal * _doRealizacji;
            set
            {
                _wartoscBruttoWal = value;
                OnPropertyChanged();
            }
        }

        public decimal Dostarczono { get => _dostarczono / Przelicznik; set => _dostarczono = value; }
        public decimal Zarezerwowano { get; set; }
        public decimal Pozostalo { get; set; }
        public decimal DoRealizacji 
        { 
            get => _doRealizacji;
            set
            {
                _doRealizacji = value;
                _doRealizacjiAktywne = Zrealizowano < Zamowiono; ;
                WartoscNetto = CenaNetto * _doRealizacji;
                WartoscBrutto = CenaBrutto * _doRealizacji;
                WartoscNettoWal = CenaNettoWal * _doRealizacji;
                WartoscBruttoWal = CenaBruttoWal * _doRealizacji;
            }
        }
        public decimal Zrealizowano { get; set; }
        public bool DoRealizacjiAktywne { get => _doRealizacjiAktywne; set { _doRealizacjiAktywne = value; OnPropertyChanged(); } }
    }
}
