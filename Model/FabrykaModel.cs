﻿using System;
using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class SkipExportExcel : Attribute { }
    public class FabrykaModel : ViewModelBase
    {
        [SkipExportExcel]
        public decimal IdFabryki { get; set; }
        public string Fabryka { get; set; }
        [SkipExportExcel]
        public decimal IdArtykulu { get; set; }
        [DisplayName("Indeks handlowy")]
        public string IndeksHandlowy { get; set; }
        [DisplayName("Nazwa artykułu")]
        public string Nazwa { get; set; }
        [DisplayName("Nazwa koloru")]
        public string Kolor { get; set; }
        [DisplayName("Numer koloru")]
        public string NumerKoloru { get; set; }
        public string Pakowanie { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get; set; }
        public decimal Dostarczono { get; set; }
        public decimal Zarezerwowano { get; set; }
        [DisplayName("Pozostało")]
        public decimal Pozostalo { get; set; }
    }
}
