﻿using System;
using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class RezerwacjeModel : ViewModelBase
    {
        [SkipExportExcel]
        public decimal Lp { get; set; }
        [SkipExportExcel]
        public decimal IdZamRezerwacji { get; set; }
        [DisplayName("Numer zamówienia rezerwacji")]
        public string NrZamRezerwacji { get; set; }
        [DisplayName("Data zamówienia rezerwacji")]
        public DateTime DataZamRezerwacji { get; set; }
        [SkipExportExcel]
        public decimal IdKlienta { get; set; }
        public string Klient { get; set; }
        [SkipExportExcel]
        public decimal IdPozZamowienia { get; set; }
        [SkipExportExcel]
        public decimal IdArtykulu { get; set; }
        [DisplayName("Indeks katalogowy")]
        public string IndeksKatalogowy { get; set; }
        [DisplayName("Indeks handlowy")]
        public string IndeksHandlowy { get; set; }
        [DisplayName("Nazwa artykułu")]
        public string Nazwa { get; set; }
        [DisplayName("Numer koloru")]
        public string Kolor { get; set; }
        public string NumerKoloru { get; set; }
        public string Pakowanie { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get; set; }
        public decimal Zrealizowano { get; set; }
        public decimal Zarezerwowano { get; set; }
        [DisplayName("Pozostało")]
        public decimal Pozostalo { get; set; }
        public string Sezon { get; set; }
    }
}
