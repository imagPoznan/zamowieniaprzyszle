﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public sealed class SqlServerModel
    {
        private List<string> _sqlAuthenticationType = new List<string> { "Windows Authentication", "SQL Server Authentication" };
        public static SqlServerModel Instance { get; set; } = new SqlServerModel();
        public SettingsModel SettingsModel { get; set; } = new SettingsModel();
        public List<string> SqlAuthenticationType { get => _sqlAuthenticationType; set => _sqlAuthenticationType = value; }
        public string SqlServer { get => SettingsModel.Instance.SqlServer; set => SettingsModel.Instance.SqlServer = value; }
        public string SqlDatabase { get => SettingsModel.Instance.SqlDatabase; set => SettingsModel.Instance.SqlDatabase = value; }
        public string SqlAuthentication { get => SettingsModel.Instance.SqlAuthentication; set => SettingsModel.Instance.SqlAuthentication = value; }
        public string SqlUser { get => SettingsModel.Instance.SqlUser; set => SettingsModel.Instance.SqlUser = value; }
        public string SqlPassword { get => SettingsModel.Instance.SqlPassword; set => SettingsModel.Instance.SqlPassword = value; }
    }
}
