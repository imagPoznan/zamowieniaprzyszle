﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.Model
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class SettingsModel
    {
        public static SettingsModel Instance { get; set; } = SettingsExtension.GetSettings();
        [DataMember]
        public string SqlServer { get; set; }
        [DataMember]
        public string SqlDatabase { get; set; }
        [DataMember]
        public string SqlAuthentication { get; set; }
        [DataMember]
        public string SqlUser { get; set; }
        [DataMember]
        public string SqlPassword { get; set; }
        [DataMember]
        public string LicencjaNumer { get; set; }
    }
}
