﻿using System;
using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class SzczegolyModel : ViewModelBase
    {
        [SkipExportExcel]
        public decimal Lp { get; set; }
        [SkipExportExcel]
        public decimal IdZamowienia { get; set; }
        [DisplayName("Numer zamówienia przyszłego")]
        public string NrZamPrzyszlego { get; set; }
        [DisplayName("Data zamówienia przyszłego")]
        public DateTime DataZamPrzyszlego { get; set; }
        public string Sezon { get; set; }
        [SkipExportExcel]
        public decimal IdFabryki { get; set; }
        public string Fabryka { get; set; }
        [SkipExportExcel]
        public decimal IdPozZamowienia { get; set; }
        [SkipExportExcel]
        public decimal IdArtykulu { get; set; }
        [DisplayName("Indeks katalogowy")]
        public string IndeksKatalogowy { get; set; }
        [DisplayName("Indeks handlowy")]
        public string IndeksHandlowy { get; set; }
        [DisplayName("Nazwa artykułu")]
        public string Nazwa { get; set; }
        public string Kolor { get; set; }
        [DisplayName("Numer koloru")]
        public string NumerKoloru { get; set; }
        public string Pakowanie { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get; set; }
        public decimal Dostarczono { get; set; }
        public decimal Zarezerwowano { get; set; }
        [DisplayName("Pozostało")]
        public decimal Pozostalo { get; set; }
    }
}
