﻿using System.ComponentModel;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.Model
{
    public class KlientModel : ViewModelBase
    {
        [SkipExportExcel]
        public decimal IdKlienta { get; set; }
        public string Klient { get; set; }
        public string NIP { get; set; }
        public string Kraj { get; set; }
        public string Adres { get; set; }
        public string KodPocztowy { get; set; }
        public string Miejscowosc { get; set; }
        public string Email { get; set; }
        [DisplayName("Zamówiono")]
        public decimal Zamowiono { get; set; }
        public decimal Zarezerwowano { get; set; }
        public decimal Zrealizowano { get; set; }
        [DisplayName("Pozostało")]
        public decimal Pozostalo { get; set; }
    }
}
