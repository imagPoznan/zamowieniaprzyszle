﻿using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle
{
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            CultureInfo.CurrentCulture = new CultureInfo("pl-PL", false);

            _ = Task.Factory.StartNew(() =>
              {
                  if (!File.Exists("Settings.bin"))
                  {
                      _ = SettingsExtension.Serialize();
                  }
              });
        }
    }
}
