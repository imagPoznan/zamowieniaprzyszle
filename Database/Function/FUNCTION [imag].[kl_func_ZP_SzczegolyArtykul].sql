/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_SzczegolyArtykul]    Script Date: 21.11.2021 17:13:42 ******/
DROP FUNCTION [imag].[kl_func_ZP_SzczegolyArtykul]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_SzczegolyArtykul]    Script Date: 21.11.2021 17:13:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-09
-- Description:	Funkcja zwraca pozycje
-- z zam�wienia przysz�ego dla indeksu
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_SzczegolyArtykul]
(@IndeksKatalogowy varchar(20)
)
RETURNS TABLE 
AS
RETURN 
(
	select	z.ID_ZAMOWIENIA IdZamPrzyszle
			,z.NUMER NrZamPrzyszle
			,convert(datetime, z.DATA-36163) DataZamPrzyszle
			,isnull(z.POLE10, '') Sezon
			,isnull(f.ID_KONTRAHENTA, 0) IdFabryki
			,isnull(f.NAZWA, '') Fabryka
			,a.NAZWA_CALA Artykul
			,a.INDEKS_HANDLOWY IndeksHandlowy
			,a.INDEKS_KATALOGOWY IndeksKatalogowy
			,isnull(a.POLE3, '') NumerKoloru
			,isnull(a.POLE2, '') NazwaKoloru
			,convert(varchar(max), a.POLE7)+'/'+convert(varchar(max), a.POLE8) Pakowanie
			,pz.ZAMOWIONO Zamowiono
			,isnull(kr.DoRezerwacji, 0) Zarezerwowano
			,pz.ZAMOWIONO-isnull(kr.DoRezerwacji, 0) Pozostalo
	from	dbo.ZAMOWIENIE z
	join	dbo.KONTRAHENT f on z.ID_KONTRAHENTA=f.ID_KONTRAHENTA
	join	dbo.POZYCJA_ZAMOWIENIA pz on z.ID_ZAMOWIENIA=pz.ID_ZAMOWIENIA
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	left	join	imag.KolejkaRezerwacji kr on pz.ID_POZYCJI_ZAMOWIENIA=kr.IdPozycjiBZD
	where	ISNULL(z.POLE10, '')<>''
	and		z.TRYBREJESTRACJI=10
	and		a.INDEKS_KATALOGOWY=@IndeksKatalogowy
)
GO


