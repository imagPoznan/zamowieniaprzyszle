/****** Object:  UserDefinedFunction [imag].[kl_func_PokazZamowieniaPrzyszlePozycja]    Script Date: 21.11.2021 16:47:45 ******/
DROP FUNCTION [imag].[kl_func_PokazZamowieniaPrzyszlePozycja]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_PokazZamowieniaPrzyszlePozycja]    Script Date: 21.11.2021 16:47:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol Łyduch
-- Create date: 2021-07-09
-- Description:	Funkcja zwraca pozycje w zamówieniach przyszłych
-- =============================================
CREATE	FUNCTION [imag].[kl_func_PokazZamowieniaPrzyszlePozycja]
		(@IdPozycji numeric
		)
RETURNS	TABLE 
AS
	RETURN
		select	p.IdPozZamowienia
				,p.Sezon
				,p.DataRealizacji
				,p.DataRealizacjiSys
				,p.ZamowionoSztuk
				,p.Jednostka
				,p.Przelicznik
				,isnull((select 1 from imag.KolejkaRezerwacji where IdPozycjiZO=@IdPozycji and IdPozycjiBZD=p.IdPozZamowienia),0) CzyZarezerwowano
				--,*
		from	imag.ZamowieniePrzyszlePozycja p
		join	imag.ZamowieniePrzyszle n on p.Sezon=n.Sezon
		where	IdArtykulu=(select ID_ARTYKULU from dbo.POZYCJA_ZAMOWIENIA where ID_POZYCJI_ZAMOWIENIA=@IdPozycji)
GO


