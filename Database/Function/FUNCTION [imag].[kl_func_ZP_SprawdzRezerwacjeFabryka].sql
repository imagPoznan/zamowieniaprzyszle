/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_SprawdzRezerwacjeFabryka]    Script Date: 21.11.2021 17:07:44 ******/
DROP FUNCTION [imag].[kl_func_ZP_SprawdzRezerwacjeFabryka]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_SprawdzRezerwacjeFabryka]    Script Date: 21.11.2021 17:07:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-09
-- Description:	Funkcja zwraca rezerwacje
-- dla indeksu katalogowego z fabryki;
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_SprawdzRezerwacjeFabryka]
(@IdFabryki numeric
,@IndeksKatalogowy varchar(20)
)
RETURNS TABLE 
AS
RETURN 
(
	select	z.ID_ZAMOWIENIA IdZamPrzyszle
			,z.NUMER NrZamPrzyszle
			,convert(datetime, z.DATA-36163) DataZamPrzyszle
			,isnull(z.POLE10, '') Sezon
			,isnull(r.ID_ZAMOWIENIA, 0) IdZamRezerwacji
			,isnull(r.NUMER, '') NrZamRezerwacji
			,convert(datetime, isnull(r.DATA-36163, 0)) DataZamRezerwacji
			,isnull(f.ID_KONTRAHENTA, 0) IdFabryki
			,isnull(f.NAZWA, '') Fabryka
			,isnull(k.ID_KONTRAHENTA, 0) IdKlienta
			,isnull(k.NAZWA, '') Klient
			,a.NAZWA_CALA Artykul
			,a.INDEKS_HANDLOWY IndeksHandlowy
			,a.INDEKS_KATALOGOWY IndeksKatalogowy
			,isnull(a.POLE3, '') NumerKoloru
			,isnull(a.POLE2, '') NazwaKoloru
			,convert(varchar(max), a.POLE7)+'/'+convert(varchar(max), a.POLE8) Pakowanie
			,pz.ZAMOWIONO Zamowiono
			,isnull(kr.DoRezerwacji, 0) DoRezerwacji
			,isnull(kr.Zarezerwowano, 0) Zarezerwowano
			,pz.ZAMOWIONO-isnull(kr.DoRezerwacji, 0) Dostepne
	from	dbo.ZAMOWIENIE z
	join	dbo.KONTRAHENT f on z.ID_KONTRAHENTA=f.ID_KONTRAHENTA
	join	dbo.POZYCJA_ZAMOWIENIA pz on z.ID_ZAMOWIENIA=pz.ID_ZAMOWIENIA
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	join	imag.KolejkaRezerwacji kr on pz.ID_POZYCJI_ZAMOWIENIA=kr.IdPozycjiBZD
	join	dbo.POZYCJA_ZAMOWIENIA pr on kr.IdPozycjiZO=pr.ID_POZYCJI_ZAMOWIENIA
	join	dbo.ZAMOWIENIE r on pr.ID_ZAMOWIENIA=r.ID_ZAMOWIENIA
	join	dbo.KONTRAHENT k on r.ID_KONTRAHENTA=k.ID_KONTRAHENTA
	where	ISNULL(z.POLE10, '')<>''
	and		z.TRYBREJESTRACJI=10
	and		f.ID_KONTRAHENTA=@IdFabryki
	and		a.INDEKS_KATALOGOWY=@IndeksKatalogowy
)
GO


