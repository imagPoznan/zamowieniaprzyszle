/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_FiltrFabryka]    Script Date: 21.11.2021 16:48:22 ******/
DROP FUNCTION [imag].[kl_func_ZP_FiltrFabryka]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_FiltrFabryka]    Script Date: 21.11.2021 16:48:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-09
-- Description:	Funkcja zwraca list� fabryk
-- do filtra fabryk;
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_FiltrFabryka]
(	
)
RETURNS TABLE 
AS
RETURN 
(
	select	distinct 
			k.ID_KONTRAHENTA IdFabryki
			,k.NAZWA Fabryka
	from	dbo.ZAMOWIENIE z
	join	dbo.KONTRAHENT k on z.ID_KONTRAHENTA=k.ID_KONTRAHENTA
	where	ISNULL(z.POLE10, '')<>''
	and		z.TRYBREJESTRACJI=10
)
GO


