/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_FiltrSezony]    Script Date: 21.11.2021 16:49:27 ******/
DROP FUNCTION [imag].[kl_func_ZP_FiltrSezony]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_FiltrSezony]    Script Date: 21.11.2021 16:49:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-09
-- Description:	Funkcja list� sezon�w
-- dla filtra sezon�w;
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_FiltrSezony]
(	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT	distinct ISNULL(POLE10, '') Sezon
	from	dbo.ZAMOWIENIE
	where	ISNULL(POLE10, '')<>''
)
GO


