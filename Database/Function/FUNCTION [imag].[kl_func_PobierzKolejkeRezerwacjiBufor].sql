/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzKolejkeRezerwacjiBufor]    Script Date: 21.11.2021 16:43:57 ******/
DROP FUNCTION [imag].[kl_func_PobierzKolejkeRezerwacjiBufor]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzKolejkeRezerwacjiBufor]    Script Date: 21.11.2021 16:43:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-07-14
-- Description:	Funkcja zwraca kolejk� rezerwacji dla wskazanej pozycji zam�wienia
-- =============================================
CREATE FUNCTION [imag].[kl_func_PobierzKolejkeRezerwacjiBufor]
(
	@IdPozycjiZamowienia numeric
)
RETURNS TABLE 
AS
	return
	select	kr.IdKolejki Lp
			,z.ID_ZAMOWIENIA IdZamowienia
			,pz.ID_POZYCJI_ZAMOWIENIA IdPozZamowienia
			,z.NUMER ZamowienieNumer
			,z.NR_ZAMOWIENIA_KLIENTA ZamowienieNrKlienta
			,z.STAN_REALIZ StanRealizacji
			,convert(datetime, z.DATA-36163) ZamowienieData
			,convert(datetime, z.DATA_REALIZACJI-36163) ZamowienieDataRealizacji
			,k.ID_KONTRAHENTA IdKontrahenta
			,k.NAZWA KontrahentNazwa
			,pz.ZAMOWIONO Zamowiono
			,pz.PRZELICZNIK Przelicznik
			,pz.JEDNOSTKA Jednostka
			,convert(decimal(16,6), pz.ZAMOWIONO) ZamowionoSzt
			,pz.ZAREZERWOWANO Zarezerwowano
			,convert(decimal(16,6), pz.ZAREZERWOWANO) ZarezerwowanoSztuk
	from	imag.KolejkaRezerwacji kr
	join	dbo.POZYCJA_ZAMOWIENIA pz on kr.IdPozycjiZO=pz.ID_POZYCJI_ZAMOWIENIA
	join	dbo.ZAMOWIENIE z on pz.ID_ZAMOWIENIA=z.ID_ZAMOWIENIA
	join	dbo.KONTRAHENT k on z.ID_KONTRAHENTA=k.ID_KONTRAHENTA
	where	kr.IdPozycjiBZD=@IdPozycjiZamowienia
GO


