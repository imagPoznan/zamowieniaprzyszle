/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzPozycjeZamowienia]    Script Date: 21.11.2021 16:45:19 ******/
DROP FUNCTION [imag].[kl_func_PobierzPozycjeZamowienia]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzPozycjeZamowienia]    Script Date: 21.11.2021 16:45:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-07-14
-- Description:	Funkcja zwraca pozycje dokumentu 
-- zam�wienia.
-- =============================================
CREATE	FUNCTION [imag].[kl_func_PobierzPozycjeZamowienia]
		(@IdPozZamowienia numeric
		)
RETURNS TABLE 
AS
RETURN 
(
	select	a.NAZWA_CALA NazwaArtykulu
			,pz.ZAMOWIONO Zamowiono
			,pz.JEDNOSTKA Jednostka
			,pz.PRZELICZNIK Przelicznik
			,a.POLE2 NazwaKoloru
			,a.POLE3 NumerKoloru
			,a.INDEKS_KATALOGOWY Indeks
			
	from	dbo.POZYCJA_ZAMOWIENIA pz
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	where	pz.ID_POZYCJI_ZAMOWIENIA=@IdPozZamowienia
)
GO


