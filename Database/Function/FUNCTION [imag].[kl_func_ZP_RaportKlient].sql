/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_RaportKlient]    Script Date: 21.11.2021 16:51:48 ******/
DROP FUNCTION [imag].[kl_func_ZP_RaportKlient]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_RaportKlient]    Script Date: 21.11.2021 16:51:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-15
-- Description:	Funkcja zwraca pozycje
-- do raportu wg klienta
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_RaportKlient]
(	
)
RETURNS TABLE 
AS
RETURN 
(
	select	distinct k.ID_KONTRAHENTA IdKlienta
			,k.NAZWA Klient
			,k.NIPL NIP
			,isnull(sk.NAZWA, isnull(k.SYM_KRAJU, '')) Kraj
			,isnull(k.ULICA_LOKAL, '') Adres
			,isnull(k.KOD_POCZTOWY, '') KodPocztowy
			,isnull(k.MIEJSCOWOSC, '') Miejscowosc
			,isnull(k.ADRES_EMAIL, '') Email
			,sum(isnull(kr.DoRezerwacji, 0)) Zamowiono
	from	dbo.KONTRAHENT k
	join	dbo.ZAMOWIENIE z on  k.ID_KONTRAHENTA=z.ID_KONTRAHENTA
	join	dbo.POZYCJA_ZAMOWIENIA pz on z.ID_ZAMOWIENIA=pz.ID_ZAMOWIENIA
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	left	join dbo.KRAJE sk on k.SYM_KRAJU=sk.SYM_KRAJU
	left	join imag.KolejkaRezerwacji kr on pz.ID_POZYCJI_ZAMOWIENIA=kr.IdPozycjiZO
	group	by k.ID_KONTRAHENTA
			,k.NAZWA
			,k.NIPL
			,sk.NAZWA
			,k.SYM_KRAJU
			,k.ULICA_LOKAL
			,k.KOD_POCZTOWY
			,k.MIEJSCOWOSC 
			,k.ADRES_EMAIL 
)
GO


