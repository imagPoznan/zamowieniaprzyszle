/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_RaportArtykul]    Script Date: 21.11.2021 16:50:05 ******/
DROP FUNCTION [imag].[kl_func_ZP_RaportArtykul]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_RaportArtykul]    Script Date: 21.11.2021 16:50:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-09
-- Description:	Funkcja zwraca pozycje
-- do raportu wg artyku�u
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_RaportArtykul]
(	
)
RETURNS TABLE 
AS
RETURN 
(
	select	a.INDEKS_KATALOGOWY IndeksKatalogowy
			,a.INDEKS_HANDLOWY IndeksHandlowy
			,a.NAZWA_CALA Nazwa
			,isnull(a.POLE3, '') NumerKoloru
			,isnull(a.POLE2, '') NazwaKoloru
			,convert(varchar(max), a.POLE7)+'/'+convert(varchar(max), a.POLE8) Pakowanie
			,sum(isnull(pz.ZAMOWIONO, 0)) Zamowiono
			,sum(isnull(kr.DoRezerwacji, 0)) Zarezerwowano
			,sum(isnull(pz.ZAMOWIONO, 0)-isnull(kr.DoRezerwacji, 0)) Pozostalo
	from	dbo.ARTYKUL a
	left	join dbo.POZYCJA_ZAMOWIENIA pz on a.ID_ARTYKULU=pz.ID_ARTYKULU
	left	join dbo.ZAMOWIENIE z on pz.ID_ZAMOWIENIA=z.ID_ZAMOWIENIA
	left	join imag.KolejkaRezerwacji kr on pz.ID_POZYCJI_ZAMOWIENIA=kr.IdPozycjiBZD
	where	isnull(z.POLE10, 'X')<>''
	and		isnull(z.TRYBREJESTRACJI, 10)=10
	and		a.RODZAJ<>'Us�uga'
	group	by a.INDEKS_KATALOGOWY
			,a.INDEKS_HANDLOWY
			,a.NAZWA_CALA
			,isnull(a.POLE3, '') 
			,isnull(a.POLE2, '') 
			,convert(varchar(max), a.POLE7)+'/'+convert(varchar(max), a.POLE8)
)
GO


