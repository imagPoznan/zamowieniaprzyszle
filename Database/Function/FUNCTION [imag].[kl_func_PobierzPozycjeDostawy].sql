/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzPozycjeDostawy]    Script Date: 21.11.2021 16:45:01 ******/
DROP FUNCTION [imag].[kl_func_PobierzPozycjeDostawy]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzPozycjeDostawy]    Script Date: 21.11.2021 16:45:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-07-14
-- Description:	Funkcja zwraca pozycje dokumentu 
-- magazynowego.
-- =============================================
CREATE	FUNCTION [imag].[kl_func_PobierzPozycjeDostawy]
		(@IdPozDostawy numeric
		)
RETURNS TABLE 
AS
RETURN
	select	ROW_NUMBER() over(partition by pdm.ID_DOK_MAGAZYNOWEGO order by pdm.ID_POZ_DOK_MAG) Lp
			,pdm.ID_POZ_DOK_MAG IdPozDokmag
			,a.NAZWA ArtykulNazwa
			,a.INDEKS_HANDLOWY ArtykulIndeksHandlowy
			,isnull(a.POLE2, '') ArtykulNazwaKoloru
			,isnull(a.POLE3, '') ArtykulNumerKoloru
			,pdm.ILOSC Ilosc
			,pdm.PRZELICZNIK Przelicznik
			,pdm.JEDNOSTKA Jednostka
			,pdm.ILOSC IloscSztuk
			,pdm.CENA_NETTO CenaNetto
			,pdm.CENA_BRUTTO CenaBrutto
			,isnull((select sum(ZAREZERWOWANO) from dbo.POZYCJA_ZAMOWIENIA where ID_DOSTAWY_REZ=pdm.ID_POZ_DOK_MAG), 0) Zarezerwowano
			,pdm.ILOSC-isnull((select sum(ZAREZERWOWANO) from dbo.POZYCJA_ZAMOWIENIA where ID_DOSTAWY_REZ=pdm.ID_POZ_DOK_MAG), 0) PozostaloDoRezerwacji
	from	dbo.POZYCJA_DOKUMENTU_MAGAZYNOWEGO pdm
	join	dbo.ARTYKUL a on pdm.ID_ARTYKULU=a.ID_ARTYKULU
	where	pdm.ID_POZ_DOK_MAG=@IdPozDostawy
GO


