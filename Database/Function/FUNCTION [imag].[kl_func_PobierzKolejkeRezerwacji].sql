/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzKolejkeRezerwacji]    Script Date: 21.11.2021 16:43:26 ******/
DROP FUNCTION [imag].[kl_func_PobierzKolejkeRezerwacji]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_PobierzKolejkeRezerwacji]    Script Date: 21.11.2021 16:43:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-07-14
-- Description:	Funkcja zwraca kolejk� rezerwacji dla wskazanej pozycji zam�wienia
-- =============================================
CREATE FUNCTION [imag].[kl_func_PobierzKolejkeRezerwacji]
(
	@IdPozDokMag numeric
)
RETURNS TABLE 
AS
	RETURN 
		select	kr.IdKolejki Lp
				,z.ID_ZAMOWIENIA IdZamowienia
				,pz.ID_POZYCJI_ZAMOWIENIA IdPozZamowienia
				,z.NUMER ZamowienieNumer
				,z.NR_ZAMOWIENIA_KLIENTA ZamowienieNrKlienta
				,z.STAN_REALIZ StanRealizacji
				,convert(datetime, z.DATA-36163) ZamowienieData
				,convert(datetime, z.DATA_REALIZACJI-36163) ZamowienieDataRealizacji
				,k.ID_KONTRAHENTA IdKontrahenta
				,k.NAZWA KontrahentNazwa
				,pz.ZAMOWIONO Zamowiono
				,pz.PRZELICZNIK Przelicznik
				,pz.JEDNOSTKA Jednostka
				,convert(decimal(16,6), pz.ZAMOWIONO) ZamowionoSzt
				,pz.ZAREZERWOWANO Zarezerwowano
				,convert(decimal(16,6), pz.ZAREZERWOWANO) ZarezerwowanoSztuk
				--,*
		from	dbo.POZYCJA_DOKUMENTU_MAGAZYNOWEGO pdm
		join	dbo.TABELA_POWIAZAN_ZAMOWIEN tpz on pdm.ID_POZ_DOK_MAG=tpz.ID_POZ_DOK_MAG
		join	imag.TabelaPowiazanZamowienPrzyszlych tpzp on tpz.ID_POZYCJI_ZAMOWIENIA=tpzp.IdPozZam
		join	imag.KolejkaRezerwacji kr on tpzp.IdPozBufor=kr.IdPozycjiBZD
		join	dbo.POZYCJA_ZAMOWIENIA pz on kr.IdPozycjiZO=pz.ID_POZYCJI_ZAMOWIENIA
		join	dbo.ZAMOWIENIE z on pz.ID_ZAMOWIENIA=z.ID_ZAMOWIENIA
		join	dbo.KONTRAHENT k on z.ID_KONTRAHENTA=k.ID_KONTRAHENTA
		join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU

		where	pdm.ID_POZ_DOK_MAG=@IdPozDokMag
GO


