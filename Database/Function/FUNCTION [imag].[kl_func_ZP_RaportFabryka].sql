/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_RaportFabryka]    Script Date: 21.11.2021 16:50:40 ******/
DROP FUNCTION [imag].[kl_func_ZP_RaportFabryka]
GO

/****** Object:  UserDefinedFunction [imag].[kl_func_ZP_RaportFabryka]    Script Date: 21.11.2021 16:50:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-11-09
-- Description:	Funkcja zwraca pozycje
-- do raportu wg fabryki
-- =============================================
CREATE FUNCTION [imag].[kl_func_ZP_RaportFabryka]
(	
)
RETURNS TABLE 
AS
RETURN 
(
	select	z.ID_KONTRAHENTA IdFabryki
			,k.NAZWA Fabryka
			,a.INDEKS_KATALOGOWY IndeksKatalogowy
			,a.INDEKS_HANDLOWY IndeksHandlowy
			,a.NAZWA_CALA Artykul
			,isnull(a.POLE3, '') NumerKoloru
			,isnull(a.POLE2, '') NazwaKoloru
			,convert(varchar(max), a.POLE7)+'/'+convert(varchar(max), a.POLE8) Pakowanie
			,sum(pz.ZAMOWIONO) Zamowiono
			,sum(isnull(kr.DoRezerwacji, 0)) Zarezerwowano
			,sum(pz.ZAMOWIONO)-sum(isnull(kr.DoRezerwacji, 0)) Pozostalo
	from	dbo.ZAMOWIENIE z
	join	dbo.KONTRAHENT k on z.ID_KONTRAHENTA=k.ID_KONTRAHENTA
	join	dbo.POZYCJA_ZAMOWIENIA pz on z.ID_ZAMOWIENIA=pz.ID_ZAMOWIENIA
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	left	join imag.KolejkaRezerwacji kr on pz.ID_POZYCJI_ZAMOWIENIA=kr.IdPozycjiBZD
	where	ISNULL(z.POLE10, '')<>''
	and		z.TRYBREJESTRACJI=10	
	group	by z.ID_KONTRAHENTA
			,k.NAZWA
			,a.INDEKS_KATALOGOWY
			,a.INDEKS_HANDLOWY
			,a.NAZWA_CALA
			,a.POLE3
			,a.POLE2
			,convert(varchar(max), a.POLE7)+'/'+convert(varchar(max), a.POLE8)
)
GO


