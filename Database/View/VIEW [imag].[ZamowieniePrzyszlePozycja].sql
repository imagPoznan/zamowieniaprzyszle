/****** Object:  View [imag].[ZamowieniePrzyszlePozycja]    Script Date: 25.06.2021 07:59:45 ******/
DROP VIEW [imag].[ZamowieniePrzyszlePozycja]
GO

/****** Object:  View [imag].[ZamowieniePrzyszlePozycja]    Script Date: 25.06.2021 07:59:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE	VIEW [imag].[ZamowieniePrzyszlePozycja]
AS
	select	ROW_NUMBER() over (partition by z.POLE10, z.POLE9 order by pz.ID_POZYCJI_ZAMOWIENIA) Lp
			,pz.ID_POZYCJI_ZAMOWIENIA IdPozZamowienia
			,pz.ID_ARTYKULU IdArtykulu
			,a.NAZWA Nazwa
			,a.INDEKS_HANDLOWY IndeksHandlowy
			,a.KOD_KRESKOWY KodKreskowy
			,z.POLE9 Fabryka
			,z.POLE10 Sezon
			,a.POLE2 NazwaKoloru
			,a.POLE3 NumerKoloru
			,cast(pz.ZAMOWIONO/pz.PRZELICZNIK as decimal(16,6)) Zamowiono
			,pz.JEDNOSTKA Jednostka
			,pz.PRZELICZNIK Przelicznik
			,cast(pz.ZAMOWIONO as decimal(16,6)) ZamowionoSztuk
			,cast(round(pz.CENA_NETTO/pz.PRZELICZNIK, 2) as decimal(14,4)) CenaNettoSztuka
			,cast(round(pz.CENA_BRUTTO/pz.PRZELICZNIK, 2) as decimal(14,4)) CenaBruttoSztuka
			,cast(round(pz.CENA_NETTO_WAL/pz.PRZELICZNIK, 2) as decimal(14,4)) CenaNettoWalSztuka
			,cast(round(pz.CENA_BRUTTO_WAL/pz.PRZELICZNIK, 2) as decimal(14,4)) CenaBruttoWalSztuka
			,cast(isnull(pz.POLE8, 0) as decimal(16,6)) Box
			,cast(isnull(pz.POLE7, 0) as decimal(16,6)) Karton
			,convert(date, convert(datetime, z.DATA_REALIZACJI-36163, 120)) DataRealizacji
			,z.DATA_REALIZACJI DataRealizacjiSys
			,pz.CENA_NETTO CenaNetto
			,pz.CENA_BRUTTO CenaBrutto
			,pz.CENA_NETTO_WAL CenaNettoWal
			,pz.CENA_BRUTTO_WAL CenaBruttoWal
			,cast(round(pz.CENA_NETTO*cast(pz.ZAMOWIONO/pz.PRZELICZNIK as decimal(16,6)), 2) as decimal (14,4)) WartoscNetto
			,cast(round(pz.CENA_BRUTTO*cast(pz.ZAMOWIONO/pz.PRZELICZNIK as decimal(16,6)), 2) as decimal (14,4)) WartoscBrutto
			,cast(round(pz.CENA_NETTO_WAL*cast(pz.ZAMOWIONO/pz.PRZELICZNIK as decimal(16,6)), 2) as decimal (14,4)) WartoscNettoWal
			,cast(round(pz.CENA_BRUTTO_WAL*cast(pz.ZAMOWIONO/pz.PRZELICZNIK as decimal(16,6)), 2) as decimal (14,4)) WartoscBruttoWal
			,isnull((select sum(Zrealizowano) from imag.TabelaPowiazanZamowienPrzyszlych where IdPozBufor=pz.ID_POZYCJI_ZAMOWIENIA), 0) Zrealizowano
	from	dbo.ZAMOWIENIE z
	join	dbo.POZYCJA_ZAMOWIENIA pz on z.ID_ZAMOWIENIA=pz.ID_ZAMOWIENIA
	join	dbo.ARTYKUL a on pz.ID_ARTYKULU=a.ID_ARTYKULU
	where	z.TRYBREJESTRACJI=10
GO


