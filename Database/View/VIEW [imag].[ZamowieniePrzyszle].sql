/****** Object:  View [imag].[ZamowieniePrzyszle]    Script Date: 25.06.2021 07:59:34 ******/
DROP VIEW [imag].[ZamowieniePrzyszle]
GO

/****** Object:  View [imag].[ZamowieniePrzyszle]    Script Date: 25.06.2021 07:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE	VIEW [imag].[ZamowieniePrzyszle]
AS
	select	z.ID_FIRMY IdFirmy
			,f.NAZWA_PELNA Firma
			,z.ID_MAGAZYNU IdMagazynu
			,m.NAZWA+' ('+m.SYMBOL+')' Magazyn
			,z.ID_KONTRAHENTA IdKontrahenta
			,k.NAZWA Dostawca
			,z.POLE10 Sezon
			--,convert(date, convert(datetime, z.DATA_REALIZACJI-36163, 120)) DataRealizacji
			--,z.DATA_REALIZACJI DataRealizacjiSys
			,z.POLE9 Fabryka
			,cast(sum(z.WARTOSC_NETTO) as decimal(14,2)) WartoscNetto
			,cast(sum(z.WARTOSC_BRUTTO) as decimal(14,2)) WartoscBrutto
			,z.SYM_WAL Waluta
			--,z.PRZELICZNIK_WAL KursWaluty
			,cast(sum(z.WARTOSC_NETTO_WAL) as decimal(14,2)) WartoscNettoWal
			,cast(sum(z.WARTOSC_BRUTTO_WAL) as decimal(14,2)) WartoscBruttoWal
	from	dbo.ZAMOWIENIE z
	join	dbo.KONTRAHENT k on z.ID_KONTRAHENTA=k.ID_KONTRAHENTA
	join	dbo.FIRMA f on z.ID_FIRMY=f.ID_FIRMY
	join	dbo.MAGAZYN m on z.ID_MAGAZYNU=m.ID_MAGAZYNU
	where	z.TRYBREJESTRACJI=10
	group	by z.ID_FIRMY
			,f.NAZWA_PELNA
			,z.ID_MAGAZYNU
			,m.NAZWA+' ('+m.SYMBOL+')'
			,z.ID_KONTRAHENTA
			,k.NAZWA
			,z.POLE10
			,z.POLE9
			,z.SYM_WAL
			--,z.PRZELICZNIK_WAL
GO


