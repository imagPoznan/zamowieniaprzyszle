/****** Object:  StoredProcedure [imag].[kl_RealizujZamowienieZD]    Script Date: 25.06.2021 08:00:40 ******/
DROP PROCEDURE [imag].[kl_RealizujZamowienieZD]
GO

/****** Object:  StoredProcedure [imag].[kl_RealizujZamowienieZD]    Script Date: 25.06.2021 08:00:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021.06.14
-- Description:	Procedura dodaj�ca zam�wienie 
-- =============================================
CREATE	PROCEDURE [imag].[kl_RealizujZamowienieZD]
		@Sezon varchar(100)
		,@Fabryka varchar(100)
		,@IdFirmy numeric
		,@IdMagazynu numeric
		,@IdUzytkownika numeric
		,@IdKontrahenta numeric
		,@DataDT varchar(10)
		,@DataRealizacjiDT varchar(10)
as
begin
	set	xact_abort, nocount on
	begin try
		--deklaracja transakcji
		begin	transaction kl_RealizujZamowienieZD
		--deklaracja zmiennych
		declare	@Message nvarchar(2048)=''
				,@IdPozBufor numeric
				,@Typ tinyint=2
				,@TrybRejestracji tinyint=0
				,@BruttoNetto varchar(6)
				,@FlagaStanu tinyint=1
				,@IdZamowienia numeric
				,@Data int

		declare	@IdPozycjiZamowienia numeric=0
				,@IdArtykulu numeric
				,@KodVat char(3)
				,@Zamowiono decimal(16,6)
				,@Zrealizowano decimal(16,6)=0
				,@Zarezerwowano decimal(16,6)=0
				,@DoRezerwacji decimal(16,6)=0
				,@CenaNetto decimal(14,4)
				,@CenaBrutto decimal(14,4)
				,@CenaNettoWal decimal(14,4)
				,@CenaBruttoWal decimal(14,4)
				,@Przelicznik decimal(16,6)
				,@Jednostka varchar(10)
				,@Narzut decimal(8,4)
				,@Opis varchar(500)=''
				,@ZnakNarzutu tinyint
				,@TrybRejestracjiPozycji tinyint=0
				,@IdDostawyRez numeric=0
				,@IdWariantuProduktu numeric=0
				,@ZnacznikCeny char(1)='k'
				,@NrSerii varchar(50)=''
				,@PozPole01 varchar(100)
				,@PozPole02 varchar(100)
				,@PozPole03 varchar(100)
				,@PozPole04 varchar(100)
				,@PozPole05 varchar(100)
				,@PozPole06 varchar(100)
				,@PozPole07 varchar(100)
				,@PozPole08 varchar(100)
				,@PozPole09 varchar(100)
				,@PozPole10 varchar(100)

		declare	@SumaNetto decimal(16,4)
				,@SumaBrutto decimal(16,4)
				,@SumaNettoWal decimal(16,4)=0
				,@SumaBruttoWal decimal(16,4)=0

		declare	@Dokument tinyint=2
				,@IdTypu numeric
				,@NumeracjaFormat varchar(50)
				,@NumeracjaOkres tinyint
				,@NumeracjaAuto tinyint
				,@NumeracjaNiezalezny tinyint

		declare	@Numer varchar(30)='<auto>'
				,@Autonumer int
				,@Zaliczka decimal(14,4)=0
				,@Priorytet tinyint=2
				,@AutoRezerwacja tinyint=0
				,@NrZamKlienta varchar(30)=''
				,@IdPracownika numeric=0
				,@PrzelicznikWal decimal(16,4)--??
				,@DataKursWal int--??
				,@SyMWal char(3)=''
				,@DokWal tinyint
				,@RabatNarzut decimal(6,2)
				,@ZnakRabatu tinyint
				,@Uwagi varchar(1000)=''
				,@InformacjeDodatkowe varchar(1000)=''
				,@OsobaZamawiajaca varchar(50)=''
				,@IdKontaktu numeric
				,@FormaPlatnosci varchar(50)=''
				,@DniPlatnosci int=0
				,@FakturaParagon tinyint=1
				,@NumerPrzesylki varchar(50)=''
				,@IdOperatoraPrzesylki numeric
				,@DataRealizacji int

		set	@Data=convert(int, convert(datetime, @DataDT, 104))+36163
		set	@DataRealizacji=convert(int, convert(datetime, @DataRealizacjiDT, 104))+36163

		--pobranie @BruttoNetto
		select	@BruttoNetto=SUMOWANIE
				,@IdTypu=ID_TYPU
		from	dbo.TYP_DOKUMENTU_MAGAZYNOWEGO
		where	ID_FIRMY=@IdFirmy
		and		SYGNATURA='ZD'

		exec	dbo.RM_DodajZamowienie_Server	@IdFirmy
												,@IdKontrahenta
												,@IdMagazynu
												,@Typ
												,@Data
												,@IdUzytkownika--@Semafor
												,@TrybRejestracji
												,@BruttoNetto
												,@FlagaStanu
												,@IdZamowienia output

		--pozycje zam�wienia
		declare	kl_pozycje cursor for
		select	pz.ID_ARTYKULU
				,pz.KOD_VAT
				,zdr.Zarezerwowano
				,pz.CENA_NETTO
				,pz.CENA_BRUTTO
				,pz.CENA_NETTO_WAL
				,pz.CENA_BRUTTO_WAL
				,pz.PRZELICZNIK
				,zdr.Jednostka
				,pz.OPIS
				,case when pz.NARZUT>=0 then 2 else 0 end
				,pz.ZNACZNIK_CENY
				,pz.POLE1
				,pz.POLE2
				,pz.POLE3
				,pz.POLE4
				,pz.POLE5
				,pz.POLE6
				,pz.POLE7
				,pz.POLE8
				,pz.POLE9
				,pz.POLE10
				,pz.NARZUT
				,pz.ID_POZYCJI_ZAMOWIENIA
		from	dbo.POZYCJA_ZAMOWIENIA pz
		join	imag.ZaznaczoneDoRealizacji zdr on pz.ID_POZYCJI_ZAMOWIENIA=zdr.IdPozycji
		where	zdr.IdUzytkownika=@IdUzytkownika
		open	kl_pozycje
		fetch	next from kl_pozycje
		into	@IdArtykulu
				,@KodVat
				,@Zamowiono
				,@CenaNetto
				,@CenaBrutto
				,@CenaNettoWal
				,@CenaBruttoWal
				,@Przelicznik
				,@Jednostka
				,@Opis
				,@ZnakNarzutu
				,@ZnacznikCeny
				,@PozPole01
				,@PozPole02
				,@PozPole03
				,@PozPole04
				,@PozPole05
				,@PozPole06
				,@PozPole07
				,@PozPole08
				,@PozPole09
				,@PozPole10
				,@Narzut
				,@IdPozBufor
		while	@@fetch_status=0
		begin
			exec	dbo.RM_DodajPozycjeZamowienia_Server	@IdPozycjiZamowienia output
															,@IdZamowienia
															,@IdArtykulu
															,@KodVat
															,@Zamowiono
															,@Zrealizowano
															,@Zarezerwowano
															,@DoRezerwacji
															,@CenaNetto
															,@CenaBrutto
															,@CenaNettoWal
															,@CenaBruttoWal
															,@Przelicznik
															,@Jednostka
															,@Narzut
															,@Opis
															,@ZnakNarzutu
															,@TrybRejestracji
															,@IdDostawyRez
															,@IdWariantuProduktu
															,@ZnacznikCeny
															,@NrSerii
			update	dbo.POZYCJA_ZAMOWIENIA
			set		POLE1=@PozPole01
					,POLE2=@PozPole02
					,POLE3=@PozPole03
					,POLE4=@PozPole04
					,POLE5=@PozPole05
					,POLE6=@PozPole06
					,POLE7=@PozPole07
					,POLE8=@PozPole08
					,POLE9=@PozPole09
					,POLE10=@PozPole10
			where	ID_POZYCJI_ZAMOWIENIA=@IdPozycjiZamowienia

			insert	into [imag].[TabelaPowiazanZamowienPrzyszlych]
			values	(@IdPozBufor
					,@IdPozycjiZamowienia
					,@Zamowiono/@Przelicznik
					,@Jednostka
					,@IdUzytkownika
					)

			fetch	next from kl_pozycje
			into	@IdArtykulu
					,@KodVat
					,@Zamowiono
					,@CenaNetto
					,@CenaBrutto
					,@CenaNettoWal
					,@CenaBruttoWal
					,@Przelicznik
					,@Jednostka
					,@Opis
					,@ZnakNarzutu
					,@ZnacznikCeny
					,@PozPole01
					,@PozPole02
					,@PozPole03
					,@PozPole04
					,@PozPole05
					,@PozPole06
					,@PozPole07
					,@PozPole08
					,@PozPole09
					,@PozPole10
					,@Narzut
					,@IdPozBufor
		end
		close	kl_pozycje
		deallocate	kl_pozycje

		exec	dbo.RM_SumujZamowienie_Server	@IdZamowienia
												,@SumaNetto output
												,@SumaBrutto output
												,@SumaNettoWal output
												,@SumaBruttoWal output

		exec	dbo.JL_PobierzFormatNumeracji_Server	@IdFirmy
														,@Dokument
														,@IdTypu
														,@IdMagazynu--@IdZasobu
														,@NumeracjaFormat output
														,@NumeracjaOkres output
														,@NumeracjaAuto output
														,@NumeracjaNiezalezny output

		exec	dbo.RM_ZatwierdzZamowienie_Server	@IdZamowienia
													,@IdKontrahenta
													,@IdTypu
													,@Numer
													,@NumeracjaFormat
													,@NumeracjaOkres
													,@NumeracjaAuto
													,@NumeracjaNiezalezny
													,@Autonumer
													,@IdFirmy
													,@IdMagazynu
													,@Data
													,@DataRealizacji
													,@Zaliczka
													,@Priorytet
													,@AutoRezerwacja
													,@NrZamKlienta
													,@Typ
													,@IdPracownika
													,@PrzelicznikWal
													,@DataKursWal
													,@SymWal
													,@DokWal
													,@FlagaStanu
													,@TrybRejestracji
													,@RabatNarzut
													,@ZnakRabatu
													,@Uwagi
													,@InformacjeDodatkowe
													,@OsobaZamawiajaca
													,@IdKontaktu
													,@FormaPlatnosci
													,@DniPlatnosci
													,@FakturaParagon
													,@NumerPrzesylki
													,@IdOperatoraPrzesylki

		delete	from imag.ZaznaczoneDoRealizacji
		--aktualizacja po dodaniu
		update	dbo.ZAMOWIENIE
		set		POLE10=@Sezon
				,Pole9=@Fabryka
		where	ID_ZAMOWIENIA=@IdZamowienia

		--zako�cz transakcj�
		commit	transaction kl_RealizujZamowienieZD
		
		return	1
	end try
	begin catch
		if @@trancount>0
			rollback	transaction kl_RealizujZamowienieZD

		declare	@ErrorNumber int=error_number()
				,@ErrorSeverity int=error_severity()
				,@ErrorState int=error_state()
				,@ErrorProcedure varchar(128)=error_procedure()
				,@ErrorLine int=error_line()
				,@ErrorMessage nvarchar(2048)=error_message()

		insert	into [imag].[Log]
				([TypLog]
				,[RodzajLog]
				,[IdFirmy]
				,[IdMagazynu]
				,[IdUzytkownika]
				,[IdObiektu]
				,[Message]
				,[ErrorNumber]
				,[ErrorSeverity]
				,[ErrorState]
				,[ErrorProcedure]
				,[ErrorLine]
				,[ErrorMessage]
				)
		values	('S'--@TypLog
				,0--b�ad
				,@IdFirmy
				,@IdMagazynu
				,@IdUzytkownika
				,@IdZamowienia
				,@Message
				,@ErrorNumber
				,@ErrorSeverity
				,@ErrorState
				,@ErrorProcedure
				,@ErrorLine
				,@ErrorMessage
				)
		--raiserror (@ErrorMessage, 16, 1)
		--dla wersji SQL>=2016
		;throw
		return 0
	end catch
end
GO


