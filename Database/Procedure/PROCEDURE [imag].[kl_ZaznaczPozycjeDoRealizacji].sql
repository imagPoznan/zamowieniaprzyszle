/****** Object:  StoredProcedure [imag].[kl_ZaznaczPozycjeDoRealizacji]    Script Date: 25.06.2021 08:00:54 ******/
DROP PROCEDURE [imag].[kl_ZaznaczPozycjeDoRealizacji]
GO

/****** Object:  StoredProcedure [imag].[kl_ZaznaczPozycjeDoRealizacji]    Script Date: 25.06.2021 08:00:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021-06-21
-- Description:	Pomocnicza procedura do tworzenia ZD
-- =============================================
CREATE	PROCEDURE [imag].[kl_ZaznaczPozycjeDoRealizacji]
		-- Add the parameters for the stored procedure here
		@IdPozZamowienia numeric
		,@DoRealizacji decimal(16,6)
		,@Jednostka varchar(10)
		,@IdUzytkownika numeric
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT	INTO [imag].[ZaznaczoneDoRealizacji]
			([IdPozycji]
			,[Zarezerwowano]
			,[Jednostka]
			,[IdUzytkownika]
			)
	VALUES	(@IdPozZamowienia
			,@DoRealizacji
			,@Jednostka
			,@IdUzytkownika
			)
END
GO


