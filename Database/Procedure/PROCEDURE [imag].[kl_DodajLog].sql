/****** Object:  StoredProcedure [imag].[kl_DodajLog]    Script Date: 25.06.2021 08:00:26 ******/
DROP PROCEDURE [imag].[kl_DodajLog]
GO

/****** Object:  StoredProcedure [imag].[kl_DodajLog]    Script Date: 25.06.2021 08:00:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Karol �yduch
-- Create date: 2021.03.02
-- Description:	Procedura dodaj�ca logi do tabeli imag.Log
-- =============================================
CREATE PROCEDURE [imag].[kl_DodajLog]
	-- Add the parameters for the stored procedure here
	@IdFirmy numeric
	,@IdMagazynu numeric
	,@IdUzytkownika numeric
	,@IdObiektu numeric
	,@TypLog char(2)
	,@RodzajLog tinyint
	,@Message nvarchar(4000)
as
begin
	set	xact_abort, nocount on
	begin try
		--deklaracja transakcji
		begin	transaction kl_DodajLog
		--deklaracja zmiennych
		declare	@IdLog numeric=0
		
		if not exists	(
						select	1
						from	imag.Log
						where	IdObiektu=@IdObiektu
						and		Message=@Message
						and		RodzajLog=@RodzajLog
						and		@TypLog in ('DP','DO','DZ','DM','DH','PP','PO','PZ','PD')
						)
		begin
			insert	into [imag].[Log]
					([TypLog]
					,[RodzajLog]
					,[IdFirmy]
					,[IdMagazynu]
					,[IdUzytkownika]
					,[IdObiektu]
					,[Message]
					)
			values	(@TypLog
					,@RodzajLog
					,@IdFirmy
					,@IdMagazynu
					,@IdUzytkownika
					,@IdObiektu
					,@Message
					)
			select	@IdLog=SCOPE_IDENTITY()
		end

		--zako�cz transakcj�
		commit	transaction kl_DodajLog
		
		return	@IdLog
	end try
	begin catch
		if @@trancount>0
			rollback	transaction kl_DodajLog

		declare	@ErrorNumber int=error_number()
				,@ErrorSeverity int=error_severity()
				,@ErrorState int=error_state()
				,@ErrorProcedure varchar(128)=error_procedure()
				,@ErrorLine int=error_line()
				,@ErrorMessage nvarchar(2048)=error_message()

		insert	into [imag].[Log]
				([TypLog]
				,[RodzajLog]
				,[IdFirmy]
				,[IdMagazynu]
				,[IdUzytkownika]
				,[IdObiektu]
				,[Message]
				,[ErrorNumber]
				,[ErrorSeverity]
				,[ErrorState]
				,[ErrorProcedure]
				,[ErrorLine]
				,[ErrorMessage]
				)
		values	('S'--@TypLog
				,0--b�ad
				,@IdFirmy
				,@IdMagazynu
				,@IdUzytkownika
				,@IdObiektu
				,@Message
				,@ErrorNumber
				,@ErrorSeverity
				,@ErrorState
				,@ErrorProcedure
				,@ErrorLine
				,@ErrorMessage
				)

		--raiserror (@ErrorMessage, 16, 1)
		--dla wersji SQL>=2016
		;throw
		return 0
	end catch
end
GO


