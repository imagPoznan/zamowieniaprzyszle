/****** Object:  Table [imag].[Log]    Script Date: 25.06.2021 08:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[Log]') AND type in (N'U'))
BEGIN
	ALTER TABLE [imag].[Log] DROP CONSTRAINT [cTypLog]

	ALTER TABLE [imag].[Log] DROP CONSTRAINT [cRodzajLog]

	ALTER TABLE [imag].[Log] DROP CONSTRAINT [cErrorMessage]

	ALTER TABLE [imag].[Log] DROP CONSTRAINT [cMessage]

	ALTER TABLE [imag].[Log] DROP CONSTRAINT [cDataLog]

	DROP TABLE [imag].[Log]
END


/****** Object:  Table [imag].[Log]    Script Date: 25.06.2021 08:03:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [imag].[Log](
	[IdLog] [int] IDENTITY(1,1) NOT NULL,
	[DataLogu] [datetime] NOT NULL,
	[TypLog] [char](2) NOT NULL,
	[RodzajLog] [tinyint] NOT NULL,
	[IdFirmy] [numeric](18, 0) NOT NULL,
	[IdMagazynu] [numeric](18, 0) NOT NULL,
	[IdUzytkownika] [numeric](18, 0) NOT NULL,
	[IdObiektu] [numeric](18, 0) NOT NULL,
	[Message] [nvarchar](4000) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorSeverity] [int] NULL,
	[ErrorState] [int] NULL,
	[ErrorProcedure] [nvarchar](128) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdLog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [imag].[Log] ADD  CONSTRAINT [cDataLog]  DEFAULT (getdate()) FOR [DataLogu]
GO

ALTER TABLE [imag].[Log] ADD  CONSTRAINT [cMessage]  DEFAULT ('') FOR [Message]
GO

ALTER TABLE [imag].[Log] ADD  CONSTRAINT [cErrorMessage]  DEFAULT ('') FOR [ErrorMessage]
GO

ALTER TABLE [imag].[Log]  WITH CHECK ADD  CONSTRAINT [cRodzajLog] CHECK  (([RodzajLog]=(3) OR [RodzajLog]=(2) OR [RodzajLog]=(1) OR [RodzajLog]=(0)))
GO

ALTER TABLE [imag].[Log] CHECK CONSTRAINT [cRodzajLog]
GO

ALTER TABLE [imag].[Log]  WITH CHECK ADD  CONSTRAINT [cTypLog] CHECK  (([TypLog]='PP' OR [TypLog]='PO' OR [TypLog]='PZ' OR [TypLog]='PD' OR [TypLog]='DP' OR [TypLog]='DO' OR [TypLog]='DZ' OR [TypLog]='DM' OR [TypLog]='DH' OR [TypLog]='GR' OR [TypLog]='S' OR [TypLog]='OD'))
GO

ALTER TABLE [imag].[Log] CHECK CONSTRAINT [cTypLog]
GO


