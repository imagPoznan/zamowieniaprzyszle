/****** Object:  Table [imag].[ZaznaczoneDoRealizacji]    Script Date: 25.06.2021 07:59:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[ZaznaczoneDoRealizacji]') AND type in (N'U'))
DROP TABLE [imag].[ZaznaczoneDoRealizacji]
GO

/****** Object:  Table [imag].[ZaznaczoneDoRealizacji]    Script Date: 25.06.2021 07:59:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [imag].[ZaznaczoneDoRealizacji](
	[IdPozycji] [numeric](18, 0) NULL,
	[Zarezerwowano] [decimal](16, 6) NULL,
	[Jednostka] [varchar](10) NULL,
	[IdUzytkownika] [numeric](18, 0) NULL
) ON [PRIMARY]
GO


