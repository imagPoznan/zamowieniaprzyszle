/****** Object:  Table [imag].[TabelaPowiazanZamowienPrzyszlych]    Script Date: 25.06.2021 07:58:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[imag].[TabelaPowiazanZamowienPrzyszlych]') AND type in (N'U'))
DROP TABLE [imag].[TabelaPowiazanZamowienPrzyszlych]
GO

/****** Object:  Table [imag].[TabelaPowiazanZamowienPrzyszlych]    Script Date: 25.06.2021 07:58:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [imag].[TabelaPowiazanZamowienPrzyszlych](
	[IdPozBufor] [numeric](18, 0) NULL,
	[IdPozZam] [numeric](18, 0) NULL,
	[Zrealizowano] [decimal](16, 6) NULL,
	[Jednostka] [varchar](10) NULL,
	[IdUzytkownika] [numeric](18, 0) NULL
) ON [PRIMARY]
GO


