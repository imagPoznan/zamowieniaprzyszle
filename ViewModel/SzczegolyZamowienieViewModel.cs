﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class SzczegolyZamowienieViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "ZamowienieSzczegoly";
        private string _nazwaPliku;
        private List<PozycjaZamowieniaModel> _listaSzczegolow;
        public List<PozycjaZamowieniaModel> ListaSzczegolow { get => _listaSzczegolow; set { _listaSzczegolow = value; OnPropertyChanged(); } }
        public SzczegolyZamowienieViewModel(decimal idZamowienia, string nazwaPliku)
        {
            ListaSzczegolow = SqlServerExtension.PobierzZamowienieSzczegoly(idZamowienia);
            _nazwaPliku = nazwaPliku;
        }

        private RelayCommand pozycjeDoExcelCommand;

        public ICommand PozycjeDoExcelCommand
        {
            get
            {
                if (pozycjeDoExcelCommand == null)
                {
                    pozycjeDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ListaSzczegolow, _nazwaPliku));
                }

                _ = MessageViewExtension.ShowMessage("Wyeksportowano do pliku Excel. ", _dialogHost);
                return pozycjeDoExcelCommand;
            }
        }
    }
}
