﻿using System.Collections.Generic;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class SzczegolyFabrykaViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "SzczegolyFabryka";
        private List<SzczegolyModel> _listaSzczegolow;
        private readonly string _nazwaPliku;
        public List<SzczegolyModel> ListaSzczegolow { get => _listaSzczegolow; set { _listaSzczegolow = value; OnPropertyChanged(); } }
        public SzczegolyFabrykaViewModel(decimal idFabryki, decimal idArtykulu, string nazwaPliku)
        {
            ListaSzczegolow = SqlServerExtension.PobierzListeSzczegolow(idFabryki, idArtykulu);
            _nazwaPliku = nazwaPliku;
        }
        private RelayCommand pozycjeDoExcelCommand;

        public ICommand PozycjeDoExcelCommand
        {
            get
            {
                if (pozycjeDoExcelCommand == null)
                {
                    pozycjeDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ListaSzczegolow, _nazwaPliku));
                }

                _ = MessageViewExtension.ShowMessage("Wyeksportowano do pliku Excel. ", _dialogHost);
                return pozycjeDoExcelCommand;
            }
        }
    }
}
