﻿using System.Collections.Generic;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class SzczegolyArtykulViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "SzczegolyArtykul";
        private List<SzczegolyModel> _listaSzczegolow;
        private readonly string _nazwaPliku;
        public List<SzczegolyModel> ListaSzczegolow { get => _listaSzczegolow; set { _listaSzczegolow = value; OnPropertyChanged(); } }
        public SzczegolyArtykulViewModel(decimal idArtykulu, string nazwaPliku)
        {
            ListaSzczegolow = SqlServerExtension.PobierzArtykulSzczegoly(idArtykulu);
            _nazwaPliku = nazwaPliku;
        }
        private RelayCommand pozycjeDoExcelCommand;

        public ICommand PozycjeDoExcelCommand
        {
            get
            {
                if (pozycjeDoExcelCommand == null)
                {
                    pozycjeDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ListaSzczegolow, _nazwaPliku));
                }

                _ = MessageViewExtension.ShowMessage("Wyeksportowano do pliku Excel. ", _dialogHost);
                return pozycjeDoExcelCommand;
            }
        }
    }
}
