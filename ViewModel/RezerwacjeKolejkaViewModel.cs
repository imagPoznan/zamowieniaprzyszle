﻿using System.Collections.Generic;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class RezerwacjeKolejkaViewModel : ViewModelBase
    {
        private List<RezerwacjeModel> _rezerwacjeKolejka;
        public List<RezerwacjeModel> RezerwacjeKolejka { get => _rezerwacjeKolejka; set { _rezerwacjeKolejka = value; OnPropertyChanged(); } }
        public RezerwacjeKolejkaViewModel(decimal idPozycji)
        {
            RezerwacjeKolejka = SqlServerExtension.PobierzKolejkeRezerwacji(idPozycji);
        }
    }
}
