﻿using System.Collections.Generic;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class RezerwacjeSezonViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "RezerwacjeSezon";
        private List<RezerwacjeModel> _listaRezerwacji;
        private readonly string _nazwaPliku;
        public List<RezerwacjeModel> ListaRezerwacji { get => _listaRezerwacji; set { _listaRezerwacji = value; OnPropertyChanged(); } }
        public RezerwacjeSezonViewModel(string sezon, decimal idFabryki, decimal idArtykulu, string nazwaPliku)
        {
            ListaRezerwacji = SqlServerExtension.PobierzListeRezerwacji(sezon, idFabryki, idArtykulu);
            _nazwaPliku = nazwaPliku;
        }
        private RelayCommand pozycjeDoExcelCommand;

        public ICommand PozycjeDoExcelCommand
        {
            get
            {
                if (pozycjeDoExcelCommand == null)
                {
                    pozycjeDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ListaRezerwacji, _nazwaPliku));
                }
                _ = MessageViewExtension.ShowMessage("Wyeksportowano do pliku Excel. ", _dialogHost);
                return pozycjeDoExcelCommand;
            }
        }
    }
}
