﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class SzczegolySezonViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "SezonSzczegoly";
        private List<SzczegolyModel> _listaSzczegolow;
        private readonly string _nazwaPliku;
        public List<SzczegolyModel> ListaSzczegolow { get => _listaSzczegolow; set { _listaSzczegolow = value; OnPropertyChanged(); } }
        public SzczegolySezonViewModel(string sezon, decimal idFabryki, decimal idArtykulu, string nazwaPliku)
        {
            ListaSzczegolow = SqlServerExtension.PobierzListeSzczegolow(sezon, idFabryki, idArtykulu);
            _nazwaPliku = nazwaPliku;
        }
        private RelayCommand pozycjeDoExcelCommand;

        public ICommand PozycjeDoExcelCommand
        {
            get
            {
                if (pozycjeDoExcelCommand == null)
                {
                    pozycjeDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ListaSzczegolow, _nazwaPliku));
                }

                _ = MessageViewExtension.ShowMessage("Wyeksportowano do pliku Excel. ", _dialogHost);
                return pozycjeDoExcelCommand;
            }
        }
    }
}
