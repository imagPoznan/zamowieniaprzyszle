﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.View;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class ZamowieniePokazViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "ZamowieniePokaz";
        public string NazwaPliku { get; set; }
        private ZamowienieModel _zamowieniePrzyszle;
        private List<PozycjaZamowieniaModel> _pozcyjeZamowienia;
        private PozycjaZamowieniaModel _zaznaczonaPozycja;
        public ZamowienieModel ZamowieniePrzyszle { get => _zamowieniePrzyszle; set { _zamowieniePrzyszle = value; OnPropertyChanged(); } }
        public List<PozycjaZamowieniaModel> PozycjeZamowienia { get => _pozcyjeZamowienia; set { _pozcyjeZamowienia = value; OnPropertyChanged(); } }
        public PozycjaZamowieniaModel ZaznaczonaPozycja { get => _zaznaczonaPozycja; set { _zaznaczonaPozycja = value; OnPropertyChanged(); } }
        public ZamowieniePokazViewModel(string sezon, decimal idFabryki, string nazwaPliku)
        {
            ZamowieniePrzyszle = SqlServerExtension.PobierzZamowieniePrzyszle(sezon, idFabryki);
            PozycjeZamowienia = SqlServerExtension.PobierzPozycjeZamowienia(sezon, idFabryki);
            NazwaPliku = nazwaPliku;
        }

        private RelayCommand rezerwacjePokazCommand;

        public ICommand RezerwacjePokazCommand
        {
            get
            {
                if (rezerwacjePokazCommand == null)
                {
                    rezerwacjePokazCommand = new RelayCommand(RezerwacjePokaz);
                }

                return rezerwacjePokazCommand;
            }
        }

        private async void RezerwacjePokaz(object commandParameter)
        {
            try
            {
                if (ZaznaczonaPozycja == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono pozycji.", _dialogHost);
                }
                else
                {
                    if (!SqlServerExtension.SprawdzCzyIstniejaRezerwacje(ZaznaczonaPozycja.IdPozZamowienia))
                    {
                        _ = MessageViewExtension.ShowWarning("Wybrana pozycja nie posiada kolejki rezerwacji.", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonaPozycja.IndeksHandlowy);
                        RezerwacjePozycjaView view = new RezerwacjePozycjaView
                        {
                            DataContext = new RezerwacjePozycjaViewModel(ZaznaczonaPozycja.IdPozZamowienia, NazwaPliku)
                        };
                        //object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void ExtendedOpenedEventHandler(object sender, DialogClosingEventArgs eventArgs) { }
        private void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter is bool parameter &&
                parameter == false) return;

            eventArgs.Cancel();
            eventArgs.Session.UpdateContent(new SampleProgressDialog());
            Task.Delay(TimeSpan.FromSeconds(0))
                .ContinueWith((t, _) => eventArgs.Session.Close(false), null,
                    TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
