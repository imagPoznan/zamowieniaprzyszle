﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class RezerwacjeZamowieniePrzyszleViewModel : ViewModelBase
    {
        private List<RezerwacjeModel> _listaRezerwacji;
        private readonly string NazwaPliku;
        public List<RezerwacjeModel> ListaRezerwacji { get => _listaRezerwacji; set { _listaRezerwacji = value; OnPropertyChanged(); } }
        public RezerwacjeZamowieniePrzyszleViewModel(string sezon, decimal idFabryki, string nazwaPliku)
        {
            ListaRezerwacji = SqlServerExtension.PobierzListeRezerwacji(sezon, idFabryki);
            NazwaPliku = nazwaPliku;
        }
    }
}
