﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class ZamowienieDodajViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "ZamowienieDodaj";
        private readonly string _nazwaPliku;
        private ZamowienieModel _zamowieniePrzyszle;
        private List<PozycjaZamowieniaModel> _pozcyjeZamowienia;
        private PozycjaZamowieniaModel _zaznaczonaPozycja;
        public ZamowienieModel ZamowieniePrzyszle { get => _zamowieniePrzyszle; set { _zamowieniePrzyszle = value; OnPropertyChanged(); } }
        public List<PozycjaZamowieniaModel> PozycjeZamowienia { get => _pozcyjeZamowienia; set { _pozcyjeZamowienia = value; OnPropertyChanged(); } }
        public PozycjaZamowieniaModel ZaznaczonaPozycja { get => _zaznaczonaPozycja; set { _zaznaczonaPozycja = value; OnPropertyChanged(); } }
        public ZamowienieDodajViewModel(string sezon, decimal idFabryki, string nazwaPliku)
        {
            ZamowieniePrzyszle = SqlServerExtension.PobierzZamowieniePrzyszle(sezon, idFabryki);
            PozycjeZamowienia = SqlServerExtension.PobierzPozycjeZamowienia(sezon, idFabryki);
            _nazwaPliku = nazwaPliku;
        }

        private RelayCommand zamowienieZapiszCommand;

        public ICommand ZamowienieZapiszCommand
        {
            get
            {
                if (zamowienieZapiszCommand == null)
                {
                    zamowienieZapiszCommand = new RelayCommand(ZamowienieZapisz);
                }

                return zamowienieZapiszCommand;
            }
        }

        private void ZamowienieZapisz(object commandParameter)
        {
            IEnumerable<PozycjaZamowieniaModel> DoRealiacji = PozycjeZamowienia.Where(i => i.DoRealizacji > 0);
            if (DoRealiacji.Count() != 0)
            {
                SqlServerExtension.WyczyszcPozycjeDoRealizacji();
                foreach (PozycjaZamowieniaModel item in DoRealiacji)
                {
                    SqlServerExtension.DodajPozycjeDoRealizacji(item.IdPozZamowienia, item.DoRealizacji, item.Jednostka, item.Przelicznik);
                }
                SqlServerExtension.UtworzZamowienieZD(ZamowieniePrzyszle.Sezon, ZamowieniePrzyszle.Fabryka, ZamowieniePrzyszle.IdFirmy, ZamowieniePrzyszle.IdMagazynu, 3000001, ZamowieniePrzyszle.IdFabryki, DateTime.Now.Date.ToString(), ZamowieniePrzyszle.DataRealizacji.ToString());
                _ = MessageViewExtension.ShowMessage("Utworzono zamówienie ZD.", _dialogHost);
            }
            else
            {
                _ = MessageViewExtension.ShowWarning("Nie wprowadzono żadnych ilości do realizacji!", _dialogHost);
            }
        }
    }
}
