﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    public class SerializeExtension<T>
    {
        public static void Serialize(string path, T obj)
        {
            if (obj == null)
            {
                return;
            }
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(T));
                ser.WriteObject(fs, obj);
            }
        }
        public static T Deserialize(string path)
        {
            try
            {
                var temp = default(T);
                if (!File.Exists(path))
                {
                    return temp;
                }
                using (FileStream fs = new FileStream(path, FileMode.Open))
                {
                    if (fs.Length <= 0) return temp;
                    XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                    DataContractSerializer ser = new DataContractSerializer(typeof(T));
                    T deserializedPerson = (T)ser.ReadObject(reader, true);
                    reader.Close();
                    fs.Close();
                    return deserializedPerson;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
