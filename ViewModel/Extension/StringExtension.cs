﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.Core;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    public class StringExtension : ViewModelBase
    {
        internal static string FormatujNazwePliku(string nazwaPliku)
        {
            try
            {
                return nazwaPliku.Replace(" ", "_").Replace("/", "_");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
