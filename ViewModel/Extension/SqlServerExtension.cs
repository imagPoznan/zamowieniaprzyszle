﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ZamowieniaPrzyszle.Model;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    public class SqlServerExtension
    {
        private static List<ZamowienieModel> listaZamowienPrzyszlych;
        private static ZamowienieModel zamowieniePrzyszle;
        private static List<PozycjaZamowieniaModel> pozycjeZamowienia;
        private static List<RezerwacjeModel> rezerwacjeKolejka;
        private static List<string> listaSezonow;
        private static List<SezonModel> raportSezon;
        private static List<FabrykaModel> listaFabryk;
        private static List<FabrykaModel> raportFabryka;
        private static List<ArtykulModel> raportArtykul;
        private static List<KlientModel> raportKlient;
        private static List<ZamowienieModel> raportZamowienie;
        private static List<PozycjaZamowieniaModel> listaPozycji;
        private static List<SzczegolyModel> listaSzczegolow;
        private static List<RezerwacjeModel> listaRezerwacji;
        /// <summary>
        /// SQL
        /// </summary>
        /// <returns></returns>
        public static bool CheckSqlConnection()
        {
            try
            {
                decimal result;
                if (GetSqlConnectionString() != "")
                {
                    using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                    {
                        sqlCnn.Open();
                        using (SqlCommand sqlCmd = new SqlCommand("select 1", sqlCnn))
                        {
                            _ = decimal.TryParse(sqlCmd.ExecuteScalar().ToString(), out result);
                        }
                    }
                    return result == 1;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static string GetSqlConnectionString()
        {
            if (SettingsModel.Instance.SqlAuthentication != null)
            {
                string sqlCnn = SettingsModel.Instance.SqlAuthentication == "Windows Authentication"
                    ? $"Data source={SettingsModel.Instance.SqlServer};Initial Catalog={SettingsModel.Instance.SqlDatabase};Trusted_Connection=True;"
                    : $"Data source={SettingsModel.Instance.SqlServer};Initial Catalog={SettingsModel.Instance.SqlDatabase};User Id={SettingsModel.Instance.SqlUser};Password={SettingsModel.Instance.SqlPassword};";
                return sqlCnn;
            }
            else
            {
                return "";
            }
        }
        public static string GetWaproDatabaseNumber()
        {
            string WaproDbNumber;
            try
            {
                if (GetSqlConnectionString() != "")
                {
                    using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                    {
                        sqlCnn.Open();
                        using (SqlCommand sqlCmd = new SqlCommand("select * from " + SettingsModel.Instance.SqlDatabase + ".dbo.WAPRODB", sqlCnn))
                        {
                            WaproDbNumber = sqlCmd.ExecuteScalar().ToString();
                        }
                    }
                    return WaproDbNumber;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// ZAM. PRZYSZŁE
        /// </summary>
        /// <returns></returns>      
        internal static List<ZamowienieModel> PobierzZamowieniaPrzyszle()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_ZamowieniaPrzyszle()", sqlCnn);
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    DataTable dt = new DataTable("ZamowieniaPrzyszle");
                    _ = da.Fill(dt);

                    listaZamowienPrzyszlych = new List<ZamowienieModel>();

                    foreach (DataRowView item in dt.DefaultView)
                    {
                        ZamowienieModel newOrder = new ZamowienieModel()
                        {
                            IdFirmy = Convert.ToDecimal(item["IdFirmy"].ToString()),
                            IdMagazynu = Convert.ToDecimal(item["IdMagazynu"].ToString()),
                            Sezon = item["Sezon"].ToString(),
                            IdFabryki = Convert.ToDecimal(item["IdFabryki"].ToString()),
                            Fabryka = item["Fabryka"].ToString(),
                            WartoscNetto = Convert.ToDecimal(item["WartoscNetto"].ToString()),
                            WartoscBrutto = Convert.ToDecimal(item["WartoscBrutto"].ToString()),
                            Waluta = item["Waluta"].ToString(),
                            WartoscNettoWal = Convert.ToDecimal(item["WartoscNettoWal"].ToString()),
                            WartoscBruttoWal = Convert.ToDecimal(item["WartoscBruttoWal"].ToString())
                        };
                        listaZamowienPrzyszlych.Add(newOrder);
                    }
                    return listaZamowienPrzyszlych;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static ZamowienieModel PobierzZamowieniePrzyszle(string sezon, decimal idFabryki)
        {
            try
            {
                zamowieniePrzyszle = new ZamowienieModel();
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.ZamowieniaPrzyszle where Sezon ='" + sezon + "' and IdFabryki=" + idFabryki.ToString(), sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            zamowieniePrzyszle.IdFirmy = Convert.ToDecimal(sqlRdr["IdFirmy"].ToString());
                            zamowieniePrzyszle.IdMagazynu = Convert.ToDecimal(sqlRdr["IdMagazynu"].ToString());
                            zamowieniePrzyszle.Sezon = sqlRdr["Sezon"].ToString();
                            zamowieniePrzyszle.IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString());
                            zamowieniePrzyszle.Fabryka = sqlRdr["Fabryka"].ToString();
                            zamowieniePrzyszle.DataRealizacji = Convert.ToDateTime(sqlRdr["DataRealizacji"].ToString());
                            zamowieniePrzyszle.WartoscNetto = Convert.ToDecimal(sqlRdr["WartoscNetto"].ToString());
                            zamowieniePrzyszle.WartoscBrutto = Convert.ToDecimal(sqlRdr["WartoscBrutto"].ToString());
                            zamowieniePrzyszle.Waluta = sqlRdr["Waluta"].ToString();
                            zamowieniePrzyszle.WartoscNettoWal = Convert.ToDecimal(sqlRdr["WartoscNettoWal"].ToString());
                            zamowieniePrzyszle.WartoscBruttoWal = Convert.ToDecimal(sqlRdr["WartoscBruttoWal"].ToString());
                        }
                    }
                    return zamowieniePrzyszle;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static List<PozycjaZamowieniaModel> PobierzPozycjeZamowienia(string sezon, decimal idFabryki)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    pozycjeZamowienia = new List<PozycjaZamowieniaModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PozycjeZamowieniaPrzyszlego('" + sezon + "' ," + idFabryki.ToString() +");", sqlCnn);
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    DataTable dt = new DataTable("PozycjaZamowienia");
                    da.Fill(dt);
                    foreach (DataRowView item in dt.DefaultView)
                    {
                        PozycjaZamowieniaModel p = new PozycjaZamowieniaModel()
                        {
                            IdPozZamowienia = Convert.ToDecimal(item["IdPozZamowienia"].ToString()),
                            Lp = Convert.ToInt32(item["Lp"].ToString()),
                            IdArtykulu = Convert.ToDecimal(item["IdArtykulu"].ToString()),
                            IndeksKatalogowy = item["IndeksKatalogowy"].ToString(),
                            IndeksHandlowy = item["IndeksHandlowy"].ToString(),
                            Nazwa = item["Nazwa"].ToString(),
                            Kolor = item["Kolor"].ToString(),
                            NumerKoloru = item["NumerKoloru"].ToString(),
                            Pakowanie = item["Pakowanie"].ToString(),
                            Jednostka = item["Jednostka"].ToString(),
                            Przelicznik = Convert.ToDecimal(item["Przelicznik"].ToString()),
                            Zamowiono = Convert.ToDecimal(item["Zamowiono"].ToString()),
                            CenaNetto = Convert.ToDecimal(item["CenaNetto"].ToString()),
                            CenaBrutto = Convert.ToDecimal(item["CenaBrutto"].ToString()),
                            CenaNettoWal = Convert.ToDecimal(item["CenaNettoWal"].ToString()),
                            CenaBruttoWal = Convert.ToDecimal(item["CenaBruttoWal"].ToString()),
                            WartoscNetto = Convert.ToDecimal(item["WartoscNetto"].ToString()),
                            WartoscBrutto = Convert.ToDecimal(item["WartoscBrutto"].ToString()),
                            WartoscNettoWal = Convert.ToDecimal(item["WartoscNettoWal"].ToString()),
                            WartoscBruttoWal = Convert.ToDecimal(item["WartoscBruttoWal"].ToString()),
                            Dostarczono = Convert.ToDecimal(item["Dostarczono"].ToString()),
                            Zarezerwowano = Convert.ToDecimal(item["Zarezerwowano"].ToString()),
                            Pozostalo = Convert.ToDecimal(item["Pozostalo"].ToString())
                        };
                        pozycjeZamowienia.Add(p);
                    }
                    return pozycjeZamowienia;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzCzyIstniejaRezerwacje(decimal idPozZamowienia)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_PozycjaRezerwacje(" + idPozZamowienia.ToString() + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<RezerwacjeModel> PobierzListeRezerwacji(decimal idPozZamowienia)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    rezerwacjeKolejka = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PozycjaRezerwacje(" + idPozZamowienia.ToString() + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel k = new RezerwacjeModel()
                            {
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            rezerwacjeKolejka.Add(k);
                        }
                    }
                    return rezerwacjeKolejka;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static List<RezerwacjeModel> PobierzKolejkeRezerwacji(decimal idPozycji)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    rezerwacjeKolejka = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_PozycjaRezerwacje(" + idPozycji.ToString() + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel k = new RezerwacjeModel()
                            {
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            rezerwacjeKolejka.Add(k);
                        }
                    }
                    return rezerwacjeKolejka;
                }
            }
            catch (Exception)
            {
                throw;
            }        
        }
        internal static void WyczyszcPozycjeDoRealizacji()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("delete from imag.ZaznaczoneDoRealizacji", sqlCnn)
                    {
                        CommandType = CommandType.Text
                    };
                    _ = sqlCmd.ExecuteNonQuery();
                    sqlCnn.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static void DodajPozycjeDoRealizacji(decimal idPozZamowienia, decimal doRealizacji, string jednostka, decimal przelicznik)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(SqlServerExtension.GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand cmd = new SqlCommand("imag.kl_ZaznaczPozycjeDoRealizacji", sqlCnn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter pIdPozZamowienia = cmd.Parameters.AddWithValue("@IdPozZamowienia", idPozZamowienia);
                    pIdPozZamowienia.Direction = ParameterDirection.Input;
                    SqlParameter pDoRealizacji = cmd.Parameters.AddWithValue("@DoRealizacji", doRealizacji);
                    pDoRealizacji.Direction = ParameterDirection.Input;
                    SqlParameter pJednostka = cmd.Parameters.AddWithValue("@Jednostka", jednostka);
                    pJednostka.Direction = ParameterDirection.Input;
                    SqlParameter pPrzelicznik = cmd.Parameters.AddWithValue("@Przelicznik", przelicznik);
                    pPrzelicznik.Direction = ParameterDirection.Input;
                    SqlParameter pIdUzytkownika = cmd.Parameters.AddWithValue("@IdUzytkownika", 3000001);
                    pIdUzytkownika.Direction = ParameterDirection.Input;
                    _ = cmd.ExecuteNonQuery();
                    sqlCnn.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static void UtworzZamowienieZD(string sezon, string fabryka, decimal idFirmy, decimal idMagazynu, decimal idUzytkownika, decimal idKontrahenta, string data, string dataRealizacji)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(SqlServerExtension.GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand cmd = new SqlCommand("imag.kl_RealizujZamowienieZD", sqlCnn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter pSezon = cmd.Parameters.AddWithValue("@Sezon", sezon);
                    pSezon.Direction = ParameterDirection.Input;
                    SqlParameter pFabryka = cmd.Parameters.AddWithValue("@Fabryka", fabryka);
                    pFabryka.Direction = ParameterDirection.Input;
                    SqlParameter pIdFirmy = cmd.Parameters.AddWithValue("@IdFirmy", idFirmy);
                    pIdFirmy.Direction = ParameterDirection.Input;
                    SqlParameter pIdMagazynu = cmd.Parameters.AddWithValue("@IdMagazynu", idMagazynu);
                    pIdMagazynu.Direction = ParameterDirection.Input;
                    SqlParameter pIdUzytkownika = cmd.Parameters.AddWithValue("@IdUzytkownika", idUzytkownika);
                    pIdUzytkownika.Direction = ParameterDirection.Input;
                    SqlParameter pIdKontrahenta = cmd.Parameters.AddWithValue("@IdKontrahenta", idKontrahenta);
                    pIdKontrahenta.Direction = ParameterDirection.Input;
                    SqlParameter pData = cmd.Parameters.AddWithValue("@DataDT", data.PadLeft(10));
                    pData.Direction = ParameterDirection.Input;
                    SqlParameter pDataRealizacji = cmd.Parameters.AddWithValue("@DataRealizacjiDT", dataRealizacji.PadLeft(10));
                    pDataRealizacji.Direction = ParameterDirection.Input;
                    _ = cmd.ExecuteNonQuery();
                    sqlCnn.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// RAPORTY - SEZON
        /// </summary>
        /// <returns></returns>
        internal static List<string> PobierzListeSezonow()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaSezonow = new List<string>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_SezonLista()", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            listaSezonow.Add(sqlRdr.GetString(0));
                        }
                    }
                    return listaSezonow;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static List<SezonModel> PobierzRaportSezon()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    raportSezon = new List<SezonModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_Sezon()", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            SezonModel s = new SezonModel()
                            {
                                Sezon = sqlRdr["Sezon"].ToString(),
                                IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                                Fabryka = sqlRdr["Fabryka"].ToString(),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            raportSezon.Add(s);
                        }
                    }
                    return raportSezon;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzCzyIstniejaSzczegoly(string sezon, decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_SezonSzczegoly('" + sezon + "', " + idFabryki.ToString() + ", " + idArtykulu.ToString() + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<SzczegolyModel> PobierzListeSzczegolow(string sezon, decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaSzczegolow = new List<SzczegolyModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_SezonSzczegoly('" + sezon + "', " + idFabryki.ToString() + ", " + idArtykulu.ToString() + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            SzczegolyModel s = new SzczegolyModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamowienia = Convert.ToDecimal(sqlRdr["IdZamowienia"].ToString()),
                                NrZamPrzyszlego = sqlRdr["NrZamPrzyszlego"].ToString(),
                                DataZamPrzyszlego = Convert.ToDateTime(sqlRdr["DataZamPrzyszlego"].ToString()),
                                Sezon = sqlRdr["Sezon"].ToString(),
                                IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                                Fabryka = sqlRdr["Fabryka"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaSzczegolow.Add(s);
                        }
                    }
                    return listaSzczegolow;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzCzyIstniejaRezerwacje(string sezon, decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_SezonRezerwacje('" + sezon + "', " + idFabryki.ToString() + ", " + idArtykulu.ToString() + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<RezerwacjeModel> PobierzListeRezerwacji(string sezon, decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaRezerwacji = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_SezonRezerwacje('" + sezon + "', " + idFabryki.ToString() + ", " + idArtykulu.ToString() + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel r = new RezerwacjeModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                IdPozZamowienia = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaRezerwacji.Add(r);
                        }
                    }
                    return listaRezerwacji;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// RAPORTY - FABRYKA
        /// </summary>
        /// <returns></returns>
        internal static List<FabrykaModel> PobierzListeFabryk()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaFabryk = new List<FabrykaModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_FabrykaLista()", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            FabrykaModel f = new FabrykaModel()
                            {
                                IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                                Fabryka = sqlRdr["Fabryka"].ToString()
                            };
                            listaFabryk.Add(f);
                        }
                    }
                    return listaFabryk;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static List<FabrykaModel> PobierzRaportFabryka()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    raportFabryka = new List<FabrykaModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_Fabryka()", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            FabrykaModel raport = new FabrykaModel()
                            {
                                IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                                Fabryka = sqlRdr["Fabryka"].ToString(),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            raportFabryka.Add(raport);
                        }
                    }
                    return raportFabryka;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzCzyIstniejaSzczegoly(decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_FabrykaSzczegoly(" + idFabryki.ToString() + ", " + idArtykulu + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<SzczegolyModel> PobierzListeSzczegolow(decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaSzczegolow = new List<SzczegolyModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_FabrykaSzczegoly(" + idFabryki.ToString() + ", " + idArtykulu + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            SzczegolyModel s = new SzczegolyModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamowienia = Convert.ToDecimal(sqlRdr["IdZamowienia"].ToString()),
                                NrZamPrzyszlego = sqlRdr["NrZamPrzyszlego"].ToString(),
                                DataZamPrzyszlego = Convert.ToDateTime(sqlRdr["DataZamPrzyszlego"].ToString()),
                                Sezon = sqlRdr["Sezon"].ToString(),
                                IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                                Fabryka = sqlRdr["Fabryka"].ToString(),
                                IdPozZamowienia = Convert.ToDecimal(sqlRdr["IdPozZamowienia"].ToString()),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaSzczegolow.Add(s);
                        }
                    }
                    return listaSzczegolow;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzCzyIstniejaRezerwacje(decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_FabrykaRezerwacje(" + idFabryki.ToString() + ", " + idArtykulu + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<RezerwacjeModel> PobierzListeRezerwacji(decimal idFabryki, decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaRezerwacji = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_FabrykaRezerwacje(" + idFabryki.ToString() + ", " + idArtykulu + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel r = new RezerwacjeModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                IdPozZamowienia = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaRezerwacji.Add(r);
                        }
                    }
                    return listaRezerwacji;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// RAPORTY - ARTYKUŁ
        /// </summary>
        /// <returns></returns>
        internal static List<ArtykulModel> PobierzRaportArtykul()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    raportArtykul = new List<ArtykulModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_Artykul()", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            ArtykulModel a = new ArtykulModel()
                            {
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            raportArtykul.Add(a);
                        }
                    }
                    return raportArtykul;
                }
            }
            catch (Exception)
            {
                throw;
            }        
        }
        internal static bool SprawdzArtykulSzczegoly(decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_ArtykulSzczegoly(" + idArtykulu + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<SzczegolyModel> PobierzArtykulSzczegoly(decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaSzczegolow = new List<SzczegolyModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_ArtykulSzczegoly(" + idArtykulu + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            SzczegolyModel s = new SzczegolyModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamowienia = Convert.ToDecimal(sqlRdr["IdZamowienia"].ToString()),
                                NrZamPrzyszlego = sqlRdr["NrZamPrzyszlego"].ToString(),
                                DataZamPrzyszlego = Convert.ToDateTime(sqlRdr["DataZamPrzyszlego"].ToString()),
                                Sezon = sqlRdr["Sezon"].ToString(),
                                IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                                Fabryka = sqlRdr["Fabryka"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaSzczegolow.Add(s);
                        }
                    }
                    return listaSzczegolow;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        internal static bool SprawdzArtykulRezerwacje(decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_ArtykulRezerwacje(" + idArtykulu + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<RezerwacjeModel> PobierzArtykulRezerwacje(decimal idArtykulu)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaRezerwacji = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_ArtykulRezerwacje(" + idArtykulu + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel r = new RezerwacjeModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                IdPozZamowienia = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaRezerwacji.Add(r);
                        }
                    }
                    return listaRezerwacji;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// RAPORTY - KLIENT
        /// </summary>
        /// <returns></returns>
        internal static List<KlientModel> PobierzRaportKlient()
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    raportKlient = new List<KlientModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_Klient()", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            KlientModel k = new KlientModel()
                            {
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                NIP = sqlRdr["NIP"].ToString(),
                                Kraj = sqlRdr["Kraj"].ToString(),
                                Adres = sqlRdr["Adres"].ToString(),
                                KodPocztowy = sqlRdr["KodPocztowy"].ToString(),
                                Miejscowosc = sqlRdr["Miejscowosc"].ToString(),
                                Email = sqlRdr["Email"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            raportKlient.Add(k);
                        }
                    }
                    return raportKlient;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzKlientRezerwacje(decimal idKlienta)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_KlientRezerwacje(" + idKlienta + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji raportu Klient. " + e.Message, "MainDialog");
                return false;
            }
        }
        internal static List<RezerwacjeModel> PobierzKlientRezerwacje(decimal idKlienta)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaRezerwacji = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_KlientRezerwacje(" + idKlienta + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel r = new RezerwacjeModel()
                            {
                                Lp = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                Sezon = sqlRdr["Sezon"].ToString(),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                IdPozZamowienia = Convert.ToDecimal(sqlRdr["Lp"].ToString()),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaRezerwacji.Add(r);
                        }
                    }
                    return listaRezerwacji;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// RAPORTY - ZAMOWIENIE
        /// </summary>
        /// <returns></returns>
        internal static List<ZamowienieModel> PobierzRaportZamowienie()
        {
            using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
            {
                raportZamowienie = new List<ZamowienieModel>();
                SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_Zamowienia()", sqlCnn);
                sqlCnn.Open();
                using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                {
                    while (sqlRdr.Read())
                    {
                        ZamowienieModel z = new ZamowienieModel()
                        {
                            IdFirmy = Convert.ToDecimal(sqlRdr["IdFirmy"].ToString()),
                            IdMagazynu = Convert.ToDecimal(sqlRdr["IdMagazynu"].ToString()),
                            IdZamPrzyszle = Convert.ToDecimal(sqlRdr["IdZamPrzyszle"].ToString()),
                            Sezon = sqlRdr["Sezon"].ToString(),
                            Numer = sqlRdr["Numer"].ToString(),
                            DataZamowienia = Convert.ToDateTime(sqlRdr["DataZamowienia"].ToString()),
                            DataRealizacji = Convert.ToDateTime(sqlRdr["DataRealizacji"].ToString()),
                            IdFabryki = Convert.ToDecimal(sqlRdr["IdFabryki"].ToString()),
                            Fabryka = sqlRdr["Fabryka"].ToString(),
                            WartoscNetto = Convert.ToDecimal(sqlRdr["WartoscNetto"].ToString()),
                            WartoscBrutto = Convert.ToDecimal(sqlRdr["WartoscBrutto"].ToString()),
                            Waluta = sqlRdr["Waluta"].ToString(),
                            WartoscNettoWal = Convert.ToDecimal(sqlRdr["WartoscNettoWal"].ToString()),
                            WartoscBruttoWal = Convert.ToDecimal(sqlRdr["WartoscBruttoWal"].ToString())
                        };
                        raportZamowienie.Add(z);
                    }
                }
                return raportZamowienie;
            }
        }
        internal static bool SprawdzCzyIstniejeZamowienie(decimal idZamowienia)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_ZamowieniaSzczegoly(" + idZamowienia.ToString() + ")", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<PozycjaZamowieniaModel> PobierzZamowienieSzczegoly(decimal idZamowienia)
        {
            try
            {
                listaPozycji = new List<PozycjaZamowieniaModel>();
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_ZamowieniaSzczegoly(" + idZamowienia.ToString() + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            PozycjaZamowieniaModel p = new PozycjaZamowieniaModel()
                            {
                                Lp = Convert.ToInt32(sqlRdr["Lp"].ToString()),
                                IdZamowienia = Convert.ToDecimal(sqlRdr["IdZamowienia"].ToString()),
                                IdPozZamowienia = Convert.ToDecimal(sqlRdr["IdPozZamowienia"].ToString()),
                                IdArtykulu = Convert.ToDecimal(sqlRdr["IdArtykulu"].ToString()),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Jednostka = sqlRdr["Jednostka"].ToString(),
                                Przelicznik = Convert.ToDecimal(sqlRdr["Przelicznik"].ToString()),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Dostarczono = Convert.ToDecimal(sqlRdr["Dostarczono"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString()),
                                CenaNetto = Convert.ToDecimal(sqlRdr["CenaNetto"].ToString()),
                                CenaBrutto = Convert.ToDecimal(sqlRdr["CenaBrutto"].ToString()),
                                CenaNettoWal = Convert.ToDecimal(sqlRdr["CenaNettoWal"].ToString()),
                                CenaBruttoWal = Convert.ToDecimal(sqlRdr["CenaBruttoWal"].ToString()),
                                WartoscNetto = Convert.ToDecimal(sqlRdr["WartoscNetto"].ToString()),
                                WartoscBrutto = Convert.ToDecimal(sqlRdr["WartoscBrutto"].ToString()),
                                WartoscNettoWal = Convert.ToDecimal(sqlRdr["WartoscNettoWal"].ToString()),
                                WartoscBruttoWal = Convert.ToDecimal(sqlRdr["WartoscBruttoWal"].ToString())
                            };
                            listaPozycji.Add(p);
                        }
                    }
                    return listaPozycji;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        internal static bool SprawdzCzyIstniejeRezerwacja(string sezon, decimal idFabryki)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand("select 1 from imag.kl_func_ZP_Rezerwacje() where Sezon='" + sezon + "' and IdFabryki=" + idFabryki.ToString() + ";", sqlCnn);
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        return sqlRdr.HasRows;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        internal static List<RezerwacjeModel> PobierzListeRezerwacji(string sezon, decimal idFabryki)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(GetSqlConnectionString()))
                {
                    listaRezerwacji = new List<RezerwacjeModel>();
                    SqlCommand sqlCmd = new SqlCommand("select * from imag.kl_func_ZamowieniaPrzyszleRezerwacje('" + sezon + "', " + idFabryki + ")", sqlCnn);
                    sqlCnn.Open();
                    using (SqlDataReader sqlRdr = sqlCmd.ExecuteReader())
                    {
                        while (sqlRdr.Read())
                        {
                            RezerwacjeModel r = new RezerwacjeModel()
                            {
                                IdZamRezerwacji = Convert.ToDecimal(sqlRdr["IdZamRezerwacji"].ToString()),
                                NrZamRezerwacji = sqlRdr["NrZamRezerwacji"].ToString(),
                                DataZamRezerwacji = Convert.ToDateTime(sqlRdr["DataZamRezerwacji"].ToString()),
                                IdKlienta = Convert.ToDecimal(sqlRdr["IdKlienta"].ToString()),
                                Klient = sqlRdr["Klient"].ToString(),
                                Nazwa = sqlRdr["Nazwa"].ToString(),
                                IndeksHandlowy = sqlRdr["IndeksHandlowy"].ToString(),
                                IndeksKatalogowy = sqlRdr["IndeksKatalogowy"].ToString(),
                                Kolor = sqlRdr["Kolor"].ToString(),
                                NumerKoloru = sqlRdr["NumerKoloru"].ToString(),
                                Pakowanie = sqlRdr["Pakowanie"].ToString(),
                                Zamowiono = Convert.ToDecimal(sqlRdr["Zamowiono"].ToString()),
                                Zrealizowano = Convert.ToDecimal(sqlRdr["Zrealizowano"].ToString()),
                                Zarezerwowano = Convert.ToDecimal(sqlRdr["Zarezerwowano"].ToString()),
                                Pozostalo = Convert.ToDecimal(sqlRdr["Pozostalo"].ToString())
                            };
                            listaRezerwacji.Add(r);
                        }
                    }
                    return listaRezerwacji;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}