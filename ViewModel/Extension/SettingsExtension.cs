﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZamowieniaPrzyszle.Model;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    public class SettingsExtension
    {
        public static SettingsExtension Instance { get; set; } = new SettingsExtension();
        public static SettingsModel GetSettings()
        {
            try
            {
                SettingsModel currentSettings = Deserialize(@"Settings.bin");
                return currentSettings ?? new SettingsModel();
            }
            catch (Exception)
            {

                throw;
            }
        }
        private static SettingsModel Deserialize(string path)
        {
            return SerializeExtension<SettingsModel>.Deserialize(@"Settings.bin");
        }
        public static bool Serialize()
        {
            try
            {
                SerializeExtension<SettingsModel>.Serialize(@"Settings.bin", SettingsModel.Instance);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
