﻿using System;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.WebReference;
//using static ZamowieniaPrzyszle.Model.LicenseModel;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    public class LicenseExtension : ViewModelBase
    {
        //public static bool GetLicense(string licencjaNumer, out LicenseModel.LicencjaStatus licencjaTyp, out DateTime? licencjaWygasa)
        public static bool GetLicense()
        {
            string program = "ZamowieniaPrzyszle";

            try
            {
                service licencja = new service { Url = "http://195.160.180.44/LicenseManager.svc?wsdl" };
                string waproDbNumber = SqlServerExtension.GetWaproDatabaseNumber();
                LicenseObject response = licencja.GetLicense(LicenseModel.Instance.LicencjaNumer, waproDbNumber, program);
                LicenseModel.Instance.LicencjaStan = response != null && response.Response ? LicenseModel.LicencjaStatus.Potwierdzona : LicenseModel.LicencjaStatus.Brak;
                LicenseModel.Instance.LicencjaWygasa = response != null && response.DataWygasnieciaSpecified
                    ? response.DataWygasniecia
                    : null;
                return response != null && response.Response;
                throw new Exception();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public static bool ActiveLicense(string licencjaNumer, out LicenseModel.LicencjaStatus licencjaTyp, out string licencjaResponse, out DateTime? licencjaWygasa)
        public static bool ActiveLicense()
        {
            string program = "ZamowieniaPrzyszle";
            try
            {
                service licencja = new service { Url = "http://195.160.180.44/LicenseManager.svc?wsdl" };
                LicenseObject response = licencja.ActiveLicense(LicenseModel.Instance.LicencjaNumer, SqlServerExtension.GetWaproDatabaseNumber(), program);                
                LicenseModel.Instance.LicencjaStan = response != null && response.Response ? LicenseModel.LicencjaStatus.Potwierdzona : LicenseModel.LicencjaStatus.Brak;
                LicenseModel.Instance.LicencjaOpis = response?.Description;
                LicenseModel.Instance.LicencjaWygasa = response != null && response.DataWygasnieciaSpecified ? response.DataWygasniecia : null;

                return response != null && response.Response;
                throw new Exception();
            }
            catch (Exception)
            {
                LicenseModel.Instance.LicencjaStan = LicenseModel.LicencjaStatus.Tymczasowa;
                LicenseModel.Instance.LicencjaWygasa = null;
                LicenseModel.Instance.LicencjaOpis = "Brak połączenia z API";
                return true;
            }
        }
    }
}
