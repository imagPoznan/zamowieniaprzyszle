﻿using MahApps.Metro.IconPacks.Converter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZamowieniaPrzyszle.Model;
using Excel = Microsoft.Office.Interop.Excel;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    public class ExcelExtension
    {
        public static DataTable ConvertToDataTable<T>(List<T> models)
        {
            try
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                PropertyInfo[] propertyInfos = typeof(T).GetProperties()
                    .Where(p => !Attribute.IsDefined(p, typeof(SkipExportExcel), false)).ToArray();
                foreach (PropertyInfo prop in propertyInfos)
                {
                    //_ = dataTable.Columns.Add(prop.Name);
                    _ = dataTable.Columns.Add(GetColumnName(prop), prop.PropertyType);
                }
                foreach (T item in models)
                {
                    object[] values = new object[propertyInfos.Count()];
                    for (int i = 0; i < propertyInfos.Count(); i++)
                    {
                        values[i] = propertyInfos[i].GetValue(item, null);
                    }
                    _ = dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z eksportem danych do pliku Excel. " + e.Message.ToString() + ".", "MainDialog");
                throw;
            }
        }
        private static string GetColumnName(PropertyInfo info)
        {
            if (Attribute.IsDefined(info, typeof(DisplayNameAttribute)))
            {
                DisplayNameAttribute dna = (DisplayNameAttribute)Attribute.GetCustomAttribute(info, typeof(DisplayNameAttribute));
                if (dna != null)
                {
                    return dna.DisplayName;
                }
            }
            return info.Name;
        }
        public static void SaveAs(DataTable dataTable, string path)
        {
            try
            {
                DataSet dataSet = new DataSet();
                dataSet.Tables.Add(dataTable);
                Excel.Application excel = new Excel.Application();
                Excel.Workbook excelWb = excel.Workbooks.Add();
                Excel._Worksheet excelWs = excelWb.Sheets[1];
                Excel.Range excelRn = excelWs.UsedRange;

                foreach (DataTable table in dataSet.Tables)
                {
                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        excelWs.Cells[1, i + 1] = table.Columns[i].ColumnName;
                    }

                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        for (int k = 0; k < table.Columns.Count; k++)
                        {
                            Excel.Range range = excelWs.Cells[j + 2, k + 1];

                            if (table.Columns[k].DataType.Name == "Decimal")
                            {
                                range.NumberFormat = "#,##0.00";
                                excelWs.Cells[j + 2, k + 1] = Convert.ToDecimal(table.Rows[j].ItemArray[k].ToString());
                            }
                            else if (table.Columns[k].DataType.Name == "DateTime")
                            {
                                excelWs.Cells[j + 2, k + 1] = Convert.ToDateTime(table.Rows[j].ItemArray[k].ToString());
                            }
                            else
                            {
                                range.NumberFormat = "@";
                                excelWs.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                            }
                        }
                    }
                }
                excelWb.SaveAs(path);
                excelWb.Close();
                excel.Quit();
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z eksportem danych do pliku Excel. " + e.Message.ToString() + ".", "MainDialog");
            }
        }
        internal static void EksportujDoExcel<T>(List<T> lista, string nazwa)
        {
            SaveFileDialog save = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Filter = "Excel 2013 File(*.xlsx)|*.xlsx",
                FileName = "" + nazwa + "_" + DateTime.Now.ToString().Replace(".", "").Replace(" ", "_").Replace(":", "")
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                SaveAs(ConvertToDataTable(lista), save.FileName);
            }
        }
    }
}
