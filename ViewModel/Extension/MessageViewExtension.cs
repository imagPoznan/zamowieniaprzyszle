﻿using MaterialDesignThemes.Wpf;
using ZamowieniaPrzyszle.View;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ZamowieniaPrzyszle.ViewModel.Extension
{
    class MessageViewExtension
    {
        public static async Task ShowMessage(string message, string dialogHost = "MainDialog")
        {
            MessageView msgBox = new MessageView()
            {
                Message = { Text = message },
                Header = { Header = "Komunikat" },
                Icon = { Kind = PackIconKind.Message, Foreground = Brushes.LightSkyBlue }
            };
            _ = await DialogHost.Show(msgBox, dialogHost);
        }
        public static async Task ShowError(string message, string dialogHost = "MainDialog")
        {
            MessageView msgBox = new MessageView()
            {
                Message = { Text = message },
                Header = { Header = "Błąd" },
                Icon = { Kind = PackIconKind.Error, Foreground = Brushes.Red }
            };
            _ = await DialogHost.Show(msgBox, dialogHost);
        }
        public static async Task ShowWarning(string message, string dialogHost = "MainDialog")
        {
            MessageView msgBox = new MessageView()
            {
                Message = { Text = message },
                Header = { Header = "Ostrzeżenie" },
                Icon = { Kind = PackIconKind.Warning, Foreground = Brushes.Orange }
            };
            _ = await DialogHost.Show(msgBox, dialogHost);
        }
    }
}
