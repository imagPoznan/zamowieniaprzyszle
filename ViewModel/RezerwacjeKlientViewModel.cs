﻿using System.Collections.Generic;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class RezerwacjeKlientViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "RezerwacjeKlient";
        private List<RezerwacjeModel> _listaRezerwacji;
        private readonly string _nazwaPliku;
        public List<RezerwacjeModel> ListaRezerwacji { get => _listaRezerwacji; set { _listaRezerwacji = value; OnPropertyChanged(); } }
        public RezerwacjeKlientViewModel(decimal idKlienta, string nazwaPliku)
        {
            ListaRezerwacji = SqlServerExtension.PobierzKlientRezerwacje(idKlienta);
            _nazwaPliku = nazwaPliku;
        }
        private RelayCommand pozycjeDoExcelCommand;

        public ICommand PozycjeDoExcelCommand
        {
            get
            {
                if (pozycjeDoExcelCommand == null)
                {
                    pozycjeDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ListaRezerwacji, _nazwaPliku));
                }
                _ = MessageViewExtension.ShowMessage("Wyeksportowano do pliku Excel. ", _dialogHost);
                return pozycjeDoExcelCommand;
            }
        }
    }
}
