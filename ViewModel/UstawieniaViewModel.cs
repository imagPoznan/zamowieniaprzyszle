﻿using System;
using System.Windows;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;
//using static ZamowieniaPrzyszle.Model.LicenseModel;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class UstawieniaViewModel : ViewModelBase
    {
        private SqlServerModel _sqlServerModel;
        private LicenseModel _licenseModel;
        public SettingsModel SettingsModel { get; set; } = new SettingsModel();
        public SqlServerModel SqlServerModel { get => _sqlServerModel; set { _sqlServerModel = value; OnPropertyChanged(); } }
        public LicenseModel LicenseModel { get => _licenseModel; set { _licenseModel = value; OnPropertyChanged(); } }
        public UstawieniaViewModel()
        {
            _sqlServerModel = SqlServerModel.Instance;
            _licenseModel = LicenseModel.Instance;
        }
        private RelayCommand zapiszUstawieniaCommand;
        public ICommand ZapiszUstawieniaCommand
        {
            get
            {
                if (zapiszUstawieniaCommand == null)
                {
                    zapiszUstawieniaCommand = new RelayCommand(ZapiszUstawienia);
                }

                return zapiszUstawieniaCommand;
            }
        }
        private void ZapiszUstawienia(object commandParameter)
        {
            _ = SettingsExtension.Serialize();
            _ = SettingsExtension.GetSettings();
            _ = MessageViewExtension.ShowMessage("Zapisano ustawienia.", "UstawieniaDialog");
        }
        private RelayCommand sprawdzPolaczenieSqlCommand;
        public ICommand SprawdzPolaczenieSqlCommand
        {
            get
            {
                if (sprawdzPolaczenieSqlCommand == null)
                {
                    sprawdzPolaczenieSqlCommand = new RelayCommand(SprawdzPolaczenieSql);
                }

                return sprawdzPolaczenieSqlCommand;
            }
        }
        private void SprawdzPolaczenieSql(object commandParameter)
        {
            if (SqlServerExtension.CheckSqlConnection())
            {
                _ = MessageViewExtension.ShowMessage("Połączenie do bazy danych WAPRO jest poprawne. Pamiętaj aby zapisać ustawienia.", "UstawieniaDialog");
            }
        }
        private RelayCommand aktywujLicencjeCommand;
        public ICommand AktywujLicencjeCommand
        {
            get
            {
                if (aktywujLicencjeCommand == null)
                {
                    aktywujLicencjeCommand = new RelayCommand(AktywujLicencje);
                }

                return aktywujLicencjeCommand;
            }
        }
        public ICommand ZamknijToOknoCommand { get;  set; }
        private void AktywujLicencje(object commandParameter)
        {
            //LicenseExtension.CheckLicense();
            //LicenseExtension.GetLicense(SettingsModel.Instance.LicencjaNumer, out LicencjaTyp licencjaTyp, out DateTime? licencjaWygasa);
            //LicenseModel.Instance.LicencjaAktywna = LicenseExtension.ActiveLicense(SettingsModel.Instance.LicencjaNumer, out LicenseModel.LicencjaStatus licencjaStan, out string licencjaTekst, out DateTime? licencjaWygasa);
            LicenseModel.Instance.LicencjaAktywna = LicenseExtension.ActiveLicense();
        }
    }
}
