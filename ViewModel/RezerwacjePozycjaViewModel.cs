﻿using System.Collections.Generic;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle.ViewModel
{
    public class RezerwacjePozycjaViewModel : ViewModelBase
    {
        private List<RezerwacjeModel> _listaRezerwacji;
        private readonly string NazwaPliku;
        public List<RezerwacjeModel> ListaRezerwacji { get => _listaRezerwacji; set { _listaRezerwacji = value; OnPropertyChanged(); } }
        public RezerwacjePozycjaViewModel(decimal idPozycji, string nazwaPliku)
        {
            ListaRezerwacji = SqlServerExtension.PobierzListeRezerwacji(idPozycji);
            NazwaPliku = nazwaPliku;
        }
    }
}
