﻿using MaterialDesignThemes.Wpf;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ZamowieniaPrzyszle.Core;
using ZamowieniaPrzyszle.Model;
using ZamowieniaPrzyszle.View;
using ZamowieniaPrzyszle.ViewModel;
using ZamowieniaPrzyszle.ViewModel.Extension;

namespace ZamowieniaPrzyszle
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly string _dialogHost = "MainDialog";
        private string _wybranySezon;
        private string _filtrSezonIndeks;
        private string _filtrSezonArtykul;
        private string _filtrSezonNumerKoloru;
        private string _filtrSezonNazwaKoloru;
        private string _filtrFabrykaIndeks;
        private string _filtrFabrykaArtykul;
        private string _filtrFabrykaNumerKoloru;
        private string _filtrFabrykaNazwaKoloru;
        private string _filtrIndeks;
        private string _filtrNazwaArtykulu;
        private string _filtrNumerKoloru;
        private string _filtrNazwaKoloru;
        private string _filtrKlientNazwa;
        private string _filtrKlientNIP;
        private string _filtrKlientKraj;
        private string _filtrKlientAdres;
        private string _filtrKlientKodPocztowy;
        private string _filtrKlientMiejscowosc;
        private string _filtrKlientEmail;
        private bool _filtrKlientZamowiono;
        private bool _filtrZarezerwowano;
        private bool _filtrZamowiono;
        private string _zamowienieWybranySezon;
        private string _filtrZamowienieNumer;
        private DateTime? _filtrZamowienieDataOd;
        private DateTime? _filtrZamowienieDataDo;

        private ZamowienieModel _zaznaczoneZamowieniePrzyszle;
        private FabrykaModel _wybranaFabryka;
        private SezonModel _zaznaczonySezon;
        private FabrykaModel _zaznaczonaFabryka;
        private ArtykulModel _zaznaczonyArtykul;
        private KlientModel _zaznaczonyKlient;
        private ZamowienieModel _zaznaczoneZamowienie;
        private string _zakresDat;

        private List<ZamowienieModel> _zamowieniePrzyszle;
        private List<string> _listaSezonow;
        private List<SezonModel> _raportSezon;
        private List<FabrykaModel> listaFabryk;
        private List<FabrykaModel> _raportFabryka;
        private List<ArtykulModel> _raportArtykul;
        private List<KlientModel> _raportKlient;
        private List<ZamowienieModel> _raportZamowienie;
        private List<string> _ListaFiltrDaty = new List<string>() { "Bieżący dzień", "Bieżący miesiąc", "Bieżący rok" };

        private RelayCommand pokazPozycjeZamowieniaCommand;
        private RelayCommand dodajZamowienieCommand;
        private RelayCommand _filtrujArtykul;
        private RelayCommand filtrujKlientaCommand;
        private RelayCommand pokazRezerwacjeSezon;
        private RelayCommand pokazSzczegolySezonCommand;
        private RelayCommand pokazSzczegolyFabrykaCommand;
        private RelayCommand pokazRezerwacjeFabrykaCommand;
        private RelayCommand pokazSzczegolyArtykulCommand;
        private RelayCommand pokazRezerwacjeArtykulCommand;
        private RelayCommand pokazRezerwacjeKlientCommand;
        private RelayCommand filtrujFabrykeCommand;
        private RelayCommand filtrujSezonCommand;
        private RelayCommand sezonDoExcelCommand;
        private RelayCommand fabrykaDoExcelCommand;
        private RelayCommand artykulDoExcelCommand;
        private RelayCommand klientDoExcelCommand;
        private RelayCommand zamowienieDoExcelCommand;
        private RelayCommand filtrujZamowienieCommand;
        private RelayCommand filtrWyczyscDaty;
        private RelayCommand pokazSzczegolyZamowienieCommand;
        private RelayCommand eksportujZDCommad;
        private RelayCommand pokazRezerwacjeZamowienieCommand;
        public UstawieniaViewModel UstawieniaViewModel { get; }
        public string WybranySezon { get => _wybranySezon; set { _wybranySezon = value; OnPropertyChanged(); } }
        public string FiltrSezonIndeks { get => _filtrSezonIndeks; set { _filtrSezonIndeks = value; OnPropertyChanged(); } }
        public string FiltrSezonArtykul { get => _filtrSezonArtykul; set { _filtrSezonArtykul = value; OnPropertyChanged(); } }
        public string FiltrSezonNumerKoloru { get => _filtrSezonNumerKoloru; set { _filtrSezonNumerKoloru = value; OnPropertyChanged(); } }
        public string FiltrSezonNazwaKoloru { get => _filtrSezonNazwaKoloru; set { _filtrSezonNazwaKoloru = value; OnPropertyChanged(); } }
        public string FiltrFabrykaIndeks { get => _filtrFabrykaIndeks; set { _filtrFabrykaIndeks = value; OnPropertyChanged(); } }
        public string FiltrFabrykaArtykul { get => _filtrFabrykaArtykul; set { _filtrFabrykaArtykul = value; OnPropertyChanged(); } }
        public string FiltrFabrykaNumerKoloru { get => _filtrFabrykaNumerKoloru; set { _filtrFabrykaNumerKoloru = value; OnPropertyChanged(); } }
        public string FiltrFabrykaNazwaKoloru { get => _filtrFabrykaNazwaKoloru; set { _filtrFabrykaNazwaKoloru = value; OnPropertyChanged(); } }
        public string FiltrIndeks { get => _filtrIndeks; set { _filtrIndeks = value; OnPropertyChanged(); } }
        public string FiltrNazwaArtykulu { get => _filtrNazwaArtykulu; set { _filtrNazwaArtykulu = value; OnPropertyChanged(); } }
        public string FiltrNumerKoloru { get => _filtrNumerKoloru; set { _filtrNumerKoloru = value; OnPropertyChanged(); } }
        public string FiltrNazwaKoloru { get => _filtrNazwaKoloru; set { _filtrNazwaKoloru = value; OnPropertyChanged(); } }
        public bool FiltrZarezerwowano { get => _filtrZarezerwowano; set { _filtrZarezerwowano = value; OnPropertyChanged(); } }
        public bool FiltrZamowiono { get => _filtrZamowiono; set { _filtrZamowiono = value; OnPropertyChanged(); } }
        public string FiltrKlientNazwa { get => _filtrKlientNazwa; set { _filtrKlientNazwa = value; OnPropertyChanged(); } }
        public string FiltrKlientNIP { get => _filtrKlientNIP; set { _filtrKlientNIP = value; OnPropertyChanged(); } }
        public string FiltrKlientKraj { get => _filtrKlientKraj; set { _filtrKlientKraj = value; OnPropertyChanged(); } }
        public string FiltrKlientAdres { get => _filtrKlientAdres; set { _filtrKlientAdres = value; OnPropertyChanged(); } }
        public string FiltrKlientKodPocztowy { get => _filtrKlientKodPocztowy; set { _filtrKlientKodPocztowy = value; OnPropertyChanged(); } }
        public string FiltrKlientMiejscowosc { get => _filtrKlientMiejscowosc; set { _filtrKlientMiejscowosc = value; OnPropertyChanged(); } }
        public string FiltrKlientEmail { get => _filtrKlientEmail; set { _filtrKlientEmail = value; OnPropertyChanged(); } }
        public bool FiltrKlientZamowiono { get => _filtrKlientZamowiono; set { _filtrKlientZamowiono = value; OnPropertyChanged(); } }
        public string ZamowienieWybranySezon { get => _zamowienieWybranySezon; set { _zamowienieWybranySezon = value; OnPropertyChanged(); } }
        public string FiltrZamowienieNumer { get => _filtrZamowienieNumer; set { _filtrZamowienieNumer = value; OnPropertyChanged(); } }
        public DateTime? FiltrZamowienieDataOd { get => _filtrZamowienieDataOd; set { _filtrZamowienieDataOd = value; OnPropertyChanged(); } }
        public DateTime? FiltrZamowienieDataDo { get => _filtrZamowienieDataDo; set { _filtrZamowienieDataDo = value; OnPropertyChanged(); } }
        public string ZakresDat { get => _zakresDat; set { _zakresDat = value; OnPropertyChanged(); } }
        public string NazwaPliku { get; set; }

        public ZamowienieModel ZaznaczoneZamowieniePrzyszle { get { return _zaznaczoneZamowieniePrzyszle; } set { _zaznaczoneZamowieniePrzyszle = value; OnPropertyChanged(); } }
        public FabrykaModel WybranaFabryka { get => _wybranaFabryka; set { _wybranaFabryka = value; OnPropertyChanged(); } }
        public SezonModel ZaznaczonySezon { get => _zaznaczonySezon; set { _zaznaczonySezon = value; OnPropertyChanged(); } }
        public FabrykaModel ZaznaczonaFabryka { get => _zaznaczonaFabryka; set { _zaznaczonaFabryka = value; OnPropertyChanged(); } }
        public ArtykulModel ZaznaczonyArtykul { get => _zaznaczonyArtykul; set { _zaznaczonyArtykul = value; OnPropertyChanged(); } }
        public KlientModel ZaznaczonyKlient { get => _zaznaczonyKlient; set { _zaznaczonyKlient = value; OnPropertyChanged(); } }
        public ZamowienieModel ZaznaczoneZamowienie { get => _zaznaczoneZamowienie; set { _zaznaczoneZamowienie = value; OnPropertyChanged(); } }

        public List<ZamowienieModel> ZamowieniePrzyszle { get => _zamowieniePrzyszle; set { _zamowieniePrzyszle = value; OnPropertyChanged(); } }
        public List<string> ListaSezonow { get => _listaSezonow; set { _listaSezonow = value; OnPropertyChanged(); } }
        public List<SezonModel> RaportSezon { get => _raportSezon; set { _raportSezon = value; OnPropertyChanged(); } }
        public List<FabrykaModel> ListaFabryk { get => listaFabryk; set => SetProperty(ref listaFabryk, value); }
        public List<FabrykaModel> RaportFabryka { get => _raportFabryka; set { _raportFabryka = value; OnPropertyChanged(); } }
        public List<ArtykulModel> RaportArtykul { get => _raportArtykul; set { _raportArtykul = value; OnPropertyChanged(); } }
        public List<KlientModel> RaportKlient { get => _raportKlient; set { _raportKlient = value; OnPropertyChanged(); } }
        public List<ZamowienieModel> RaportZamowienie { get => _raportZamowienie; set { _raportZamowienie = value; OnPropertyChanged(); } }
        public List<string> ListaFiltrDaty { get => _ListaFiltrDaty; set { _ListaFiltrDaty = value; OnPropertyChanged(); } }

        public ICommand PokazZamowienieCommand
        {
            get
            {
                if (pokazPozycjeZamowieniaCommand == null)
                {
                    pokazPozycjeZamowieniaCommand = new RelayCommand(ZamowieniePokaz);
                }

                return pokazPozycjeZamowieniaCommand;
            }
        }
        public ICommand DodajZamowienieCommand
        {
            get
            {
                if (dodajZamowienieCommand == null)
                {
                    dodajZamowienieCommand = new RelayCommand(ZamowienieDodaj);
                }

                return dodajZamowienieCommand;
            }
        }
        public ICommand UstawieniaCommand => new RelayCommand(Ustawienia);
        public ICommand OdswiezCommand => new RelayCommand(Odswiez); 
        public ICommand FiltrujArtykulCommand
        {
            get
            {
                if (_filtrujArtykul == null)
                {
                    _filtrujArtykul = new RelayCommand(FiltrujArtykul);
                }

                return _filtrujArtykul;
            }
        }
        public ICommand FiltrujKlientaCommand
        {
            get
            {
                if (filtrujKlientaCommand == null)
                {
                    filtrujKlientaCommand = new RelayCommand(FiltrujKlienta);
                }

                return filtrujKlientaCommand;
            }
        }
        public ICommand PokazRezerwacjeSezonCommand
        {
            get
            {
                if (pokazRezerwacjeSezon == null)
                {
                    pokazRezerwacjeSezon = new RelayCommand(PokazRezerwacjeSezon);
                }

                return pokazRezerwacjeSezon;
            }
        }
        public ICommand PokazSzczegolySezonCommand
        {
            get
            {
                if (pokazSzczegolySezonCommand == null)
                {
                    pokazSzczegolySezonCommand = new RelayCommand(PokazSzczegolySezon);
                }

                return pokazSzczegolySezonCommand;
            }
        }
        public ICommand PokazSzczegolyFabrykaCommand
        {
            get
            {
                if (pokazSzczegolyFabrykaCommand == null)
                {
                    pokazSzczegolyFabrykaCommand = new RelayCommand(PokazSzczegolyFabryka);
                }

                return pokazSzczegolyFabrykaCommand;
            }
        }
        public ICommand PokazRezerwacjeFabrykaCommand
        {
            get
            {
                if (pokazRezerwacjeFabrykaCommand == null)
                {
                    pokazRezerwacjeFabrykaCommand = new RelayCommand(PokazRezerwacjeFabryka);
                }

                return pokazRezerwacjeFabrykaCommand;
            }
        }
        public ICommand PokazSzczegolyArtykulCommand
        {
            get
            {
                if (pokazSzczegolyArtykulCommand == null)
                {
                    pokazSzczegolyArtykulCommand = new RelayCommand(PokazSzczegolyArtykul);
                }

                return pokazSzczegolyArtykulCommand;
            }
        }
        public ICommand PokazRezerwacjeArtykulCommand
        {
            get
            {
                if (pokazRezerwacjeArtykulCommand == null)
                {
                    pokazRezerwacjeArtykulCommand = new RelayCommand(PokazRezerwacjeArtykul);
                }

                return pokazRezerwacjeArtykulCommand;
            }
        }
        public ICommand PokazRezerwacjeKlientCommand
        {
            get
            {
                if (pokazRezerwacjeKlientCommand == null)
                {
                    pokazRezerwacjeKlientCommand = new RelayCommand(PokazRezerwacjeKlient);
                }

                return pokazRezerwacjeKlientCommand;
            }
        }
        public ICommand FiltrujFabrykeCommand
        {
            get
            {
                if (filtrujFabrykeCommand == null)
                {
                    filtrujFabrykeCommand = new RelayCommand(FiltrujFabryke);
                }

                return filtrujFabrykeCommand;
            }
        }
        public ICommand FiltrujSezonCommand
        {
            get
            {
                if (filtrujSezonCommand == null)
                {
                    filtrujSezonCommand = new RelayCommand(FiltrujSezon);
                }

                return filtrujSezonCommand;
            }
        }
        public ICommand SezonDoExcelCommand
        {
            get
            {
                if (sezonDoExcelCommand == null)
                {
                    sezonDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(RaportSezon, "Sezon"));
                }

                return sezonDoExcelCommand;
            }
        }
        public ICommand FabrykaDoExcelCommand
        {
            get
            {
                if (fabrykaDoExcelCommand == null)
                {
                    fabrykaDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(RaportFabryka, "Fabryka"));
                }

                return fabrykaDoExcelCommand;
            }
        }
        public ICommand ArtykulDoExcelCommand
        {
            get
            {
                if (artykulDoExcelCommand == null)
                {
                    artykulDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(RaportArtykul, "Artykul"));
                }

                return artykulDoExcelCommand;
            }
        }
        public ICommand KlientDoExcelCommand
        {
            get
            {
                if (klientDoExcelCommand == null)
                {
                    klientDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(RaportKlient, "Klient"));
                }

                return klientDoExcelCommand;
            }
        }
        public ICommand ZamowienieDoExcelCommand
        {
            get
            {
                if (zamowienieDoExcelCommand == null)
                {
                    zamowienieDoExcelCommand = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(RaportZamowienie, "Zamowienie"));
                }

                return zamowienieDoExcelCommand;
            }
        }
        public ICommand FiltrujZamowienieCommand
        {
            get
            {
                if (filtrujZamowienieCommand == null)
                {
                    filtrujZamowienieCommand = new RelayCommand(FiltrujZamowienie);
                }

                return filtrujZamowienieCommand;
            }
        }
        public ICommand FiltrWyczyscDatyCommand
        {
            get
            {
                if (filtrWyczyscDaty == null)
                {
                    filtrWyczyscDaty = new RelayCommand(FiltrWyczyscDaty);
                }

                return filtrWyczyscDaty;
            }
        }
        public ICommand PokazSzczegolyZamowienieCommand
        {
            get
            {
                if (pokazSzczegolyZamowienieCommand == null)
                {
                    pokazSzczegolyZamowienieCommand = new RelayCommand(PokazSzczegolyZamowienie);
                }

                return pokazSzczegolyZamowienieCommand;
            }
        }
        public ICommand EksportujZDCommad
        {
            get
            {
                if (eksportujZDCommad == null)
                {
                    eksportujZDCommad = new RelayCommand(_ => ExcelExtension.EksportujDoExcel(ZamowieniePrzyszle, "ZamowieniaPrzyszle"));
                }

                return eksportujZDCommad;
            }
        }
        public ICommand PokazRezerwacjeZamowienieCommand
        {
            get
            {
                if (pokazRezerwacjeZamowienieCommand == null)
                {
                    pokazRezerwacjeZamowienieCommand = new RelayCommand(PokazRezerwacjeZamowienie);
                }

                return pokazRezerwacjeZamowienieCommand;
            }
        }

        public MainWindowViewModel()
        {
            LicenseModel.Instance.LicencjaAktywna = LicenseExtension.GetLicense();

            if (SqlServerExtension.CheckSqlConnection())
            {
                ZamowieniePrzyszle = SqlServerExtension.PobierzZamowieniaPrzyszle();
                ListaSezonow = SqlServerExtension.PobierzListeSezonow();
                ListaFabryk = SqlServerExtension.PobierzListeFabryk();
                FiltrujSezon("");
                FiltrujFabryke("");
                FiltrujArtykul("");
                FiltrujKlienta("");
                FiltrujZamowienie("");
            }
            else
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem danych. Sprawdź ustawienia.", _dialogHost);
            }
        }

        private async void ZamowieniePokaz(object commandParameter)
        {
            try
            {
                if (ZaznaczoneZamowieniePrzyszle == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono zamówienia przyszłego. ", _dialogHost);
                }
                else
                {
                    NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczoneZamowieniePrzyszle.Sezon + " " + ZaznaczoneZamowieniePrzyszle.Fabryka);
                    ZamowieniePokazView v = new ZamowieniePokazView
                    {
                        DataContext = new ZamowieniePokazViewModel(ZaznaczoneZamowieniePrzyszle.Sezon, ZaznaczoneZamowieniePrzyszle.IdFabryki, NazwaPliku)
                    };
                    object result = await DialogHost.Show(v, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem pozycji. " + e.Message, _dialogHost);
            }
        }
        private async void ZamowienieDodaj(object commandParameter)
        {
            try
            {
                if (ZaznaczoneZamowieniePrzyszle == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono zamówienia przyszłego. ", _dialogHost);
                }
                else
                {
                    NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczoneZamowieniePrzyszle.Sezon + " " + ZaznaczoneZamowieniePrzyszle.Fabryka);
                    ZamowienieDodajView v = new ZamowienieDodajView
                    {
                        DataContext = new ZamowienieDodajViewModel(ZaznaczoneZamowieniePrzyszle.Sezon, ZaznaczoneZamowieniePrzyszle.IdFabryki, NazwaPliku)
                    };
                    object result = await DialogHost.Show(v, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem pozycji. " + e.Message, _dialogHost);
            }
        }
        private async void Ustawienia(object o)
        {
            UstawieniaView view = new UstawieniaView
            {
                 DataContext = new UstawieniaViewModel()
            };
            object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
        }
        private void Zamknij()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }
        private void ExtendedOpenedEventHandler(object sender, DialogOpenedEventArgs eventargs) { }
        private void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (eventArgs.Parameter is bool parameter &&
                parameter == false) return;

            eventArgs.Cancel();
            eventArgs.Session.UpdateContent(new SampleProgressDialog());
            Task.Delay(TimeSpan.FromSeconds(0))
                .ContinueWith((t, _) => eventArgs.Session.Close(false), null,
                    TaskScheduler.FromCurrentSynchronizationContext());
        }
        private void Odswiez(object commandParameter)
        {
            if (SqlServerExtension.CheckSqlConnection())
            {
                ZamowieniePrzyszle = SqlServerExtension.PobierzZamowieniaPrzyszle();
                ListaSezonow = SqlServerExtension.PobierzListeSezonow();
                ListaFabryk = SqlServerExtension.PobierzListeFabryk();
                FiltrujSezon("");
                FiltrujFabryke("");
                FiltrujArtykul("");
                FiltrujKlienta("");
                FiltrujZamowienie("");
            }
            else
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem danych. Sprawdź ustawienia.", _dialogHost);
            }
        }
        private void FiltrujSezon(object commandParameter)
        {
            try
            {
                IEnumerable<SezonModel> s = SqlServerExtension.PobierzRaportSezon();
                if (!string.IsNullOrWhiteSpace(WybranySezon))
                {
                    s = s.Where(x => x.Sezon.ToUpper().Contains((WybranySezon ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrSezonIndeks))
                {
                    s = s.Where(x => x.IndeksHandlowy.ToUpper().Contains((FiltrSezonIndeks ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrSezonArtykul))
                {
                    s = s.Where(x => x.Nazwa.ToUpper().Contains((FiltrSezonArtykul ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrSezonNumerKoloru))
                {
                    s = s.Where(x => x.NumerKoloru.ToUpper().Contains((FiltrSezonNumerKoloru ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrSezonNazwaKoloru))
                {
                    s = s.Where(x => x.Kolor.ToUpper().Contains((FiltrSezonNazwaKoloru ?? "").ToUpper()));
                }
                RaportSezon = s.ToList();
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem z filtrowaniem w zakładce sezon. " + e.Message, _dialogHost);
            }
        }
        private void FiltrujFabryke(object commandParameter)
        {
            try
            {
                IEnumerable<FabrykaModel> f = SqlServerExtension.PobierzRaportFabryka();
                if (WybranaFabryka != null)
                {
                    if (!string.IsNullOrWhiteSpace(WybranaFabryka.Fabryka))
                    {
                        f = f.Where(x => x.Fabryka.ToUpper().Contains(WybranaFabryka.Fabryka.ToUpper()));
                    }
                }
                if (!string.IsNullOrWhiteSpace(FiltrFabrykaIndeks))
                {
                    f = f.Where(x => x.IndeksHandlowy.ToUpper().Contains((FiltrFabrykaIndeks ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrFabrykaArtykul))
                {
                    f = f.Where(x => x.Nazwa.ToUpper().Contains((FiltrFabrykaArtykul ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrFabrykaNumerKoloru))
                {
                    f = f.Where(x => x.NumerKoloru.ToUpper().Contains((FiltrFabrykaNumerKoloru ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrFabrykaNazwaKoloru))
                {
                    f = f.Where(x => x.Kolor.ToUpper().Contains((FiltrFabrykaNazwaKoloru ?? "").ToUpper()));
                }
                RaportFabryka = f.ToList();
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem z filtrowaniem w zakładce fabryka. " + e.Message, _dialogHost);
            }
        }
        private void FiltrujArtykul(object commandParameter)
        {
            try
            {
                IEnumerable<ArtykulModel> artykul = SqlServerExtension.PobierzRaportArtykul();
                if (!string.IsNullOrWhiteSpace(FiltrIndeks))
                {
                    artykul = artykul.Where(x => x.IndeksHandlowy.ToUpper().Contains((FiltrIndeks ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrNazwaArtykulu))
                {
                    artykul = artykul.Where(x => x.Nazwa.ToUpper().Contains((FiltrNazwaArtykulu ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrNumerKoloru))
                {
                    artykul = artykul.Where(x => x.NumerKoloru.ToUpper().Contains((FiltrNumerKoloru ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrNazwaKoloru))
                {
                    artykul = artykul.Where(x => x.Kolor.ToUpper().Contains((FiltrNazwaKoloru ?? "").ToUpper()));
                }
                if (FiltrZarezerwowano)
                {
                    artykul = artykul.Where(x => x.Zarezerwowano > 0);
                }
                if (FiltrZamowiono)
                {
                    artykul = artykul.Where(x => x.Zamowiono > 0);
                }
                RaportArtykul = artykul.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void FiltrujKlienta(object commandParameter)
        {
            try
            {
                IEnumerable<KlientModel> klient = SqlServerExtension.PobierzRaportKlient();
                if (!string.IsNullOrWhiteSpace(FiltrKlientNazwa))
                {
                    klient = klient.Where(X => X.Klient.ToUpper().Contains((FiltrKlientNazwa ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrKlientNIP))
                {
                    klient = klient.Where(x => x.NIP.ToUpper().Contains(((FiltrKlientNIP ?? "").Replace(" ","").Replace("-","") ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrKlientKraj))
                {
                    klient = klient.Where(x => x.Kraj.ToUpper().Contains((FiltrKlientKraj ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrKlientAdres))
                {
                    klient = klient.Where(x => x.Adres.ToUpper().Contains((FiltrKlientAdres ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrKlientKodPocztowy))
                {
                    klient = klient.Where(x => x.KodPocztowy.ToUpper().Contains(((FiltrKlientKodPocztowy ?? "").Replace(" ", "").Replace("-", "") ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrKlientMiejscowosc))
                {
                    klient = klient.Where(x => x.Miejscowosc.ToUpper().Contains((FiltrKlientMiejscowosc ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(FiltrKlientEmail))
                {
                    klient = klient.Where(x => x.Email.ToUpper().Contains((FiltrKlientEmail ?? "").ToUpper()));
                }
                if (FiltrKlientZamowiono == true)
                {
                    klient = klient.Where(x => x.Zamowiono > 0);
                }
                RaportKlient = klient.ToList();
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z filtrowaniem listy kontrahentów. " + e.Message, _dialogHost);
            }
        }
        private void FiltrujZamowienie(object commandParameter)
        {
            try
            {
                IEnumerable<ZamowienieModel> z = SqlServerExtension.PobierzRaportZamowienie();
                if (ZamowienieWybranySezon != null)
                {
                    if (!string.IsNullOrWhiteSpace(ZamowienieWybranySezon))
                    {
                        z = z.Where(x => x.Sezon.ToUpper().Contains((ZamowienieWybranySezon ?? "").ToUpper()));
                    }
                }
                if (!string.IsNullOrWhiteSpace(FiltrZamowienieNumer))
                {
                    z = z.Where(x => x.Numer.ToUpper().Contains((FiltrZamowienieNumer ?? "").ToUpper()));
                }
                if (!string.IsNullOrWhiteSpace(ZakresDat))
                {
                    DateTime data = DateTime.Today;
                    if (ZakresDat.ToString() == "Bieżący dzień")
                    {
                        FiltrZamowienieDataOd = data;
                        FiltrZamowienieDataDo = data;
                    }
                    else if (ZakresDat.ToString() == "Week")
                    {
                        FiltrZamowienieDataOd = DateTime.Today;
                        FiltrZamowienieDataDo = DateTime.Today;
                    }
                    else if (ZakresDat.ToString() == "Bieżący miesiąc")
                    {
                        FiltrZamowienieDataOd = new DateTime(data.Year, data.Month, 1);
                        FiltrZamowienieDataDo = new DateTime(Convert.ToDateTime(FiltrZamowienieDataOd).Year, Convert.ToDateTime(FiltrZamowienieDataOd).Month, Convert.ToDateTime(FiltrZamowienieDataOd).AddMonths(1).AddDays(-1).Day);
                    }
                    else if (ZakresDat.ToString() == "Bieżący rok")
                    {
                        FiltrZamowienieDataOd = new DateTime(data.Year, 1, 1);
                        FiltrZamowienieDataDo = new DateTime(data.Year, 12, 31);
                    }
                }
                else
                {
                    FiltrWyczyscDaty(commandParameter);
                }
                if (!string.IsNullOrWhiteSpace(FiltrZamowienieDataOd.ToString()))
                {
                    z = z.Where(x => x.DataZamowienia >= FiltrZamowienieDataOd);
                }
                if (!string.IsNullOrWhiteSpace(FiltrZamowienieDataDo.ToString()))
                {
                    z = z.Where(x => x.DataZamowienia <= FiltrZamowienieDataDo);
                }
                RaportZamowienie = z.ToList();
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem z filtrowaniem w zakładce zamówienie. " + e.Message, _dialogHost);
            }
        }
        private async void PokazSzczegolySezon(object commandParameter)
        {
            try
            {
                if (ZaznaczonySezon == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzCzyIstniejaSzczegoly(ZaznaczonySezon.Sezon, ZaznaczonySezon.IdFabryki, ZaznaczonySezon.IdArtykulu) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Pozycja " + ZaznaczonySezon.Nazwa + " nie rezerwacji od klientów na sezon " + ZaznaczonySezon.Sezon + " w fabryce " + ZaznaczonySezon.Fabryka + ". ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonySezon.Sezon + " " + ZaznaczonySezon.IndeksHandlowy);
                        SzczegolySezonView view = new SzczegolySezonView
                        {
                            DataContext = new SzczegolySezonViewModel(ZaznaczonySezon.Sezon, ZaznaczonySezon.IdFabryki, ZaznaczonySezon.IdArtykulu, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji sezonu. " + e.Message, _dialogHost);
            }
        }
        private async void PokazRezerwacjeSezon(object commandParameter)
        {
            try
            {
                if (ZaznaczonySezon == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzCzyIstniejaRezerwacje(ZaznaczonySezon.Sezon, ZaznaczonySezon.IdFabryki, ZaznaczonySezon.IdArtykulu) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Pozycja " + ZaznaczonySezon.Nazwa + " nie rezerwacji od klientów na sezon " + ZaznaczonySezon.Sezon + ". ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonySezon.Sezon + " " + ZaznaczonySezon.IndeksHandlowy);
                        RezerwacjeSezonView view = new RezerwacjeSezonView
                        {
                            DataContext = new RezerwacjeSezonViewModel(ZaznaczonySezon.Sezon, ZaznaczonySezon.IdFabryki, ZaznaczonySezon.IdArtykulu, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji sezonu. " + e.Message, _dialogHost);
            }
        }
        private async void PokazSzczegolyFabryka(object commandParameter)
        {
            try
            {
                if (ZaznaczonaFabryka == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzCzyIstniejaSzczegoly(ZaznaczonaFabryka.IdFabryki, ZaznaczonaFabryka.IdArtykulu) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Pozycja " + ZaznaczonaFabryka.Nazwa + " nie rezerwacji od klientów do fabryki " + ZaznaczonaFabryka.Fabryka + ". ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonaFabryka.Fabryka + " " + ZaznaczonaFabryka.IndeksHandlowy);
                        SzczegolyFabrykaView view = new SzczegolyFabrykaView
                        {
                            DataContext = new SzczegolyFabrykaViewModel(ZaznaczonaFabryka.IdFabryki, ZaznaczonaFabryka.IdArtykulu, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji sezonu. " + e.Message, _dialogHost);
            }
        }
        private async void PokazRezerwacjeFabryka(object commandParameter)
        {
            try
            {
                if (ZaznaczonaFabryka == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzCzyIstniejaRezerwacje(ZaznaczonaFabryka.IdFabryki, ZaznaczonaFabryka.IdArtykulu) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Pozycja " + ZaznaczonaFabryka.Nazwa + " nie rezerwacji od klientów do fabryki " + ZaznaczonaFabryka.Fabryka + ". ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonaFabryka.Fabryka + " " + ZaznaczonaFabryka.IndeksHandlowy);
                        RezerwacjeFabrykaView view = new RezerwacjeFabrykaView
                        {
                            DataContext = new RezerwacjeFabrykaViewModel(ZaznaczonaFabryka.IdFabryki, ZaznaczonaFabryka.IdArtykulu, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji wg fabryki. " + e.Message, _dialogHost);
            }
        }
        private async void PokazSzczegolyArtykul(object commandParameter)
        {
            try
            {
                if (ZaznaczonyArtykul == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzArtykulSzczegoly(ZaznaczonyArtykul.IdArtykulu) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Pozycja " + ZaznaczonyArtykul.Nazwa + " nie występuje na zamówieniu przyszłym. ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonyArtykul.IndeksHandlowy + " " + ZaznaczonyArtykul.Nazwa);
                        SzczegolyArtykulView view = new SzczegolyArtykulView
                        {
                            DataContext = new SzczegolyArtykulViewModel(ZaznaczonyArtykul.IdArtykulu, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji sezonu. " + e.Message, _dialogHost);
            }
        }
        private async void PokazRezerwacjeArtykul(object commandParameter)
        {
            try
            {
                if (ZaznaczonyArtykul == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzArtykulRezerwacje(ZaznaczonyArtykul.IdArtykulu) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Pozycja " + ZaznaczonyArtykul.Nazwa + " jeszcze nie została zarezerwowana przez klientów. ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonyArtykul.IndeksHandlowy + " " + ZaznaczonyArtykul.Nazwa);
                        RezerwacjeFabrykaView view = new RezerwacjeFabrykaView
                        {
                            DataContext = new RezerwacjeArtykulViewModel(ZaznaczonyArtykul.IdArtykulu, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji wg artykuło. " + e.Message, _dialogHost);
            }
        }
        private async void PokazRezerwacjeKlient(object commandParameter)
        {
            try
            {
                if (ZaznaczonyKlient == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (!SqlServerExtension.SprawdzKlientRezerwacje(ZaznaczonyKlient.IdKlienta))
                    {
                        _ = MessageViewExtension.ShowWarning("Klient " + ZaznaczonyKlient.Klient + " nie złożył jeeszcze rezerwacji. ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczonyKlient.Klient);
                        RezerwacjeKlientView view = new RezerwacjeKlientView
                        {
                            DataContext = new RezerwacjeKlientViewModel(ZaznaczonyKlient.IdKlienta, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji wg klienta. " + e.Message, _dialogHost);
            }
        }
        private void FiltrWyczyscDaty(object commandParameter)
        {
            try
            {
                ZakresDat = null;
                FiltrZamowienieDataOd = null;
                FiltrZamowienieDataDo = null;
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem z filtrowaniem w zakładce zamówienie. " + e.Message, _dialogHost);
            }
        }
        private async void PokazSzczegolyZamowienie(object commandParameter)
        {
            try
            {
                if (ZaznaczoneZamowienie == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnego zamówienia. ", _dialogHost);
                }
                else
                {
                    if (SqlServerExtension.SprawdzCzyIstniejeZamowienie(ZaznaczoneZamowienie.IdZamPrzyszle) == false)
                    {
                        _ = MessageViewExtension.ShowWarning("Zamówienie " + ZaznaczoneZamowienie.Numer + " nie posiada pozycji. ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczoneZamowienie.Fabryka + " " + ZaznaczoneZamowienie.Numer + " " + ZaznaczoneZamowienie.Sezon);
                        SzczegolyZamowienieView view = new SzczegolyZamowienieView
                        {
                            DataContext = new SzczegolyZamowienieViewModel(ZaznaczoneZamowienie.IdZamPrzyszle, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pokazaniem szczegółów zamówienia. " + e.Message, _dialogHost);
            }
        }
        private async void PokazRezerwacjeZamowienie(object commandParameter)
        {
            try
            {
                if (ZaznaczoneZamowieniePrzyszle == null)
                {
                    _ = MessageViewExtension.ShowWarning("Nie zaznaczono żadnej pozycji. ", _dialogHost);
                }
                else
                {
                    if (!SqlServerExtension.SprawdzCzyIstniejeRezerwacja(ZaznaczoneZamowieniePrzyszle.Sezon, ZaznaczoneZamowieniePrzyszle.IdFabryki))
                    {
                        _ = MessageViewExtension.ShowWarning("Zamówienie na sezon '" + ZaznaczoneZamowieniePrzyszle.Sezon + " z fabryki " + ZaznaczoneZamowieniePrzyszle.Fabryka + " nie posiada jeszcze rezerwacji. ", _dialogHost);
                    }
                    else
                    {
                        NazwaPliku = StringExtension.FormatujNazwePliku(ZaznaczoneZamowieniePrzyszle.Sezon + " " + ZaznaczoneZamowieniePrzyszle.Fabryka);
                        RezerwacjeZamowieniePrzyszleView view = new RezerwacjeZamowieniePrzyszleView
                        {
                            DataContext = new RezerwacjeZamowieniePrzyszleViewModel(ZaznaczoneZamowieniePrzyszle.Sezon, ZaznaczoneZamowieniePrzyszle.IdFabryki, NazwaPliku)
                        };
                        object result = await DialogHost.Show(view, _dialogHost, ExtendedOpenedEventHandler, ExtendedClosingEventHandler);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageViewExtension.ShowError("Wystąpił problem z pobraniem rezerwacji wg zamówienia przyszłego. " + e.Message, _dialogHost);
            }
        }
    }
}
